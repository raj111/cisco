
import tornado.web
import tornado.gen
import time
from functools import partial
import os
from concurrent.futures import ThreadPoolExecutor

def long_blocking_function(index, sleep_time):
    print ("Entering run counter:%s" % (index,))
    time.sleep(sleep_time)
    print ("Exiting run counter:%s" % (index,))
    return 'Keyyyyy' + index

class BarHandler(tornado.web.RequestHandler):

    counter = 0

    def initialize(self, executor):
        self.executor = executor

    @tornado.gen.coroutine
    def get(self):
        global counter
        BarHandler.counter += 1
        current_counter = str(BarHandler.counter)
        print ("ABOUT to spawn thread for counter:%s" % (current_counter,))
        future_result = yield self.executor.submit(long_blocking_function,
                                              index=current_counter,
                                              sleep_time=2)
        print ("DONE executing thread for long function")
        self.write(future_result)

class BarHandler2(tornado.web.RequestHandler):

    counter = 0

    def initialize(self):
        pass

    def get(self):
        global counter
        BarHandler2.counter += 1
        current_counter = str(BarHandler2.counter)
        print ("ABOUT to work for counter:%s" % (current_counter,))
        print ("Entering run counter:%s" % (current_counter,))
        time.sleep(10)
        print ("Exiting run counter:%s" % (current_counter,))
        future_result = 'Keyyyyy' + current_counter
        self.write(future_result)

class Application(tornado.web.Application):
    def __init__(self):
        handlers = [(r"/bar", BarHandler, dict(executor=ThreadPoolExecutor(max_workers=1000))),
                    (r"/bar2", BarHandler2),
                   ]
        settings = dict(
            debug=True,
            static_path=os.path.join(os.path.dirname(__file__), "static"),
            template_path=os.path.join(os.path.dirname(__file__), "templates"),
        )
        tornado.web.Application.__init__(self, handlers, **settings)

def main():
    application = Application()
    application.listen(8888)

    tornado.ioloop.IOLoop.instance().start()

if __name__ == "__main__":
    print ("Starting server.")
    main()


