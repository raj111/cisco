# Webserver Modules
import tornado.httpserver
import tornado.ioloop
import tornado.web
import tornado.websocket
import tornado.gen
import motor.motor_tornado

import os
import Utility.LoginUtility as LogIn
import Utility.DecoderUtility as Decoder





ClientPool = {} # websocket ID to client class





class UI_Handler(tornado.web.RequestHandler):
    def get(self):
        print("[INF] [HTML] [LogIn] Page request")
        self.render("UI/HTML/Login.html")





class LogIn_WebSocketHandler(tornado.websocket.WebSocketHandler):
    def open(self):
        self.db = self.settings['db']
        print("[INF] [SOCK] [LogIn] New client connected")

    def on_message(self, message):
        print("[INF] [SOCK] [LogIn] New message recieved - " + message)
        message = message.split(',')
        LogIn.Options[message[1]](self, message, ClientPool)
        
    def on_close(self):
        print("[INF] [SOCK] [LogIn] Client Clossed")
        if id(self) in ClientPool.keys() and "Router" not in ClientPool[id(self)].keys():
            print("cleanup")





class DC_Handler(tornado.web.RequestHandler):
    def get(self, input):
        print("[INF] [HTML] [LogIn] Page request")
        print(ClientPool)
        self.render("DC/HTML/DCIndex.html")





class Decoder_WebSocketHandler(tornado.websocket.WebSocketHandler):
    def open(self):
        print(ClientPool)
        self.db = self.settings['db']
        print("[INF] [SOCK] [DEC] New client connected")

    def on_message(self, message):
        print("[INF] [SOCK] [DEC] New message recieved - " + message)
        message = message.split(',')
        Decoder.Options[message[1]](self, message, ClientPool)
        
    def on_close(self):
        print("[INF] [SOCK] [DEC] Client Clossed")
        if id(self) in ClientPool.keys():
            del ClientPool[id(self)]

if __name__ == "__main__":

    db = motor.motor_tornado.MotorClient('localhost', 27017)
    handlers = [(r"/", UI_Handler),
                (r'/LogInWebSocket', LogIn_WebSocketHandler),
                (r"/DC/(\w+)", DC_Handler),
                (r'/DecoderWebSocket', Decoder_WebSocketHandler),
                (r'/DC/DWN/file/(.*)', tornado.web.StaticFileHandler, {'path': os.path.join(os.path.dirname(__file__), "download")})
				]
    settings = {'debug':True,'template_path': 'frontend','static_path': os.path.join(os.path.dirname(__file__), "frontend"),'db':db}

    app = tornado.web.Application(handlers, **settings)

    http_server = tornado.httpserver.HTTPServer(app)

    if(len(os.sys.argv) < 2):
        http_server.listen(8090)
        print("[ERR] No port number specified, please execute: python <filename> <port number>")
        print("[INF] Starting server at port 8090")
    else:
        http_server.listen(int(os.sys.argv[1]))
        print("[INF] Starting server at port " + os.sys.argv[1])		
    tornado.ioloop.IOLoop.instance().start()
