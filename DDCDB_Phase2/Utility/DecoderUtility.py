import tornado.gen
import time
import re
import rns_decoder




@tornado.gen.coroutine
def ExchangeSessionId(websock, message, ClientPool):
    print(str(message[0]) + " -> " + str(id(websock)))
    ClientPool[id(websock)] = ClientPool.pop(int(message[0]))
    status = "Chip names database fail"
    try:
        future = websock.db.decoder.collection_names()
        result = yield future
        if result != None:
            status = "S"
    except Exception as error:
        print(str(error))
        status = str(error)

    if status == "S":
        status = ','.join(result)
        msg = str(id(websock)) + "," + "c" + ',' + 'S' + ',' + status 
    else:
        msg = str(id(websock)) + "," + "c" + ',' + 'U' + "," + status
    websock.write_message(msg)

@tornado.gen.coroutine
def SelectAsic(websock, message, ClientPool):
    status = "Chip names database fail"
    try:
        ClientPool[id(websock)]["collection"] = websock.db.decoder[message[2].split('_')[0]]
        print(message[2])
        future = ClientPool[id(websock)]["collection"].find_one({'_id':message[2]})
        result = yield future
        if result != None:
            status = "S"
    except Exception as error:
        print(str(error))
        status = str(error)
    
    if status == "S" :
        status = result['child_name']
        if result['name'].rfind('=') == -1:
            name = result['name']
        else:
            name = result['name'][result['name'].rfind('=')+1:]
        msg = str(id(websock)) + "," + "g" + "," + "S"  + ',' + name + ',' + str(result['instances']) + ',' + ','.join(status)
    else:
        msg = str(id(websock)) + "," + "g" + "," + "U" + ',' + status
    websock.write_message(msg)

@tornado.gen.coroutine
def Suggestions(websock, message, ClientPool):
    status = "Chip names database fail"
    try:
        future = ClientPool[id(websock)]["collection"].find_one({'_id':message[2]})
        result = yield future
        if result != None:
            status = "S"
    except Exception as error:
        print(str(error))
        status = str(error)
    
    if status == "S" :
        status = result['child_name']
        print('multiple instance ')
        print(result['instances'])
        if result['name'].rfind('=') == -1:
            name = result['name']
        else:
            name = result['name'][result['name'].rfind('=')+1:]
        msg = str(id(websock)) + "," + "g" + "," + "S"  + ',' + name + ',' + str(result['instances']) + ',' + ','.join(status)
    else:
        msg = str(id(websock)) + "," + "g" + "," + "U" + ',' + status
    websock.write_message(msg)

def ParseSingleRegisterValue(websock, queryname, cmddump, htmltext):
    pass
    
@tornado.gen.coroutine
def Query(websock, message, ClientPool):
    querylist = message[2:]
    print(querylist)
    l = int(len(querylist)/2)
    htmltext = ""
    for i in range(l):
        print (querylist[i] + ' - ' + querylist[l+i])
        if querylist[l+i] == 'l':
            ClientPool[id(websock)]['Router'].Cmd("read " + '.'.join(querylist[i].split('=')[1:]))
        else:
            ClientPool[id(websock)]['Router'].Cmd("read " + '.'.join(querylist[i].split('=')[1:]) + " -family -zero")
        time.sleep(0.1)
        cmddump =  ClientPool[id(websock)]['Router'].Read()
        if "read: No Such Register:" in cmddump:
            print("read: No Such Register:")
        else:
            cmddump = cmddump.split('\n')
            if querylist[l+i] == 'l':
                hexvalue = ""
                for j in cmddump:
                    if "s0:" in j:
                        hexvalue = j
                        break
                if hexvalue == "":
                    return "Cannot Parse Value"
    
                hexvalue = hexvalue.split()
                name = re.sub("[\(\[].*?[\)\]]", "", querylist[i])
                future = ClientPool[id(websock)]["collection"].find_one({'_id': name})
                result = yield future
                print(result)
                htmltext += '<li class = "tree"><lsc href="#">' + hexvalue[3].replace('.', ' . ') + '</lsc><ul><li class = "tree"><table>'
                tagstart = '<th id= '
                tagmid = ' bgcolor= '
                tagend = ' >'
                hexvalue, splitindex, fieldlabels = rns_decoder.decodeval(hexvalue[2], result['start'], result['end'])
                szreg, sz, grupno = len(hexvalue), 0, 0
                
                while (sz < szreg):
                    sztemp, grupnotemp = sz, grupno
                    htmltext += '<tr>'
                    cellid = "name"
                    while True:
                        if fieldlabels[sztemp] == 0: # reserved
                            col = '"#f9cccc"'
                            cellid = '0' 
                        else:
                            if sztemp%2 == 0: # non-reserved
                                col = '"#FFFFFF"' # even
                            else:
                                col = '"#dddddd"' # odd
                            grupnotemp += 1
                            cellid = str(grupnotemp)

                        print(col)
                        print(sztemp)

                        if splitindex[sztemp]-1 == splitindex[sztemp+1] :
                            value = str(splitindex[sztemp]-1)
                        else:
                            value = str(splitindex[sztemp]-1) + "-" + str(splitindex[sztemp+1])
                        
                        htmltext += tagstart + result['name'] + '-' + cellid  + tagmid + col + tagend + value + '</th>'
                        sztemp += 1
                        if sztemp%6 == 0 or sztemp==szreg:                    
                            break

                    htmltext += '</tr><tr>'
                    sztemp, grupnotemp = sz, grupno
                    while True:                
                        if fieldlabels[sztemp] == 0: # reserved
                            col = '"#f9cccc"'
                            value = "reserved"
                            cellid = '0' 
                        else:
                            if sztemp%2 == 0: # non-reserved
                                col = '"#FFFFFF"' # even
                            else:
                                col = '"#dddddd"' # odd
                            value = result['subfield_name'][grupnotemp]
                            grupnotemp += 1
                            cellid = str(grupnotemp)

                        print(col)
                        print(sztemp)

                        htmltext += tagstart + result['name'] + '-' + cellid  + tagmid + col + tagend + value + '</th>'
                        sztemp += 1
                        if sztemp%6 == 0 or sztemp==szreg:                    
                            break

                    htmltext += '</tr><tr>'
                    sztemp, grupnotemp = sz, grupno
                    while True:
                        if fieldlabels[sztemp] == 0: # reserved
                            col = '"#f9cccc"'
                            cellid = '0' 
                        else:
                            if sztemp%2 == 0: # non-reserved
                                col = '"#FFFFFF"' # even
                            else:
                                col = '"#dddddd"' # odd
                            grupnotemp += 1
                            cellid = str(grupnotemp)

                        print(col)
                        print(sztemp)

                        htmltext += tagstart + result['name'] + '-' + cellid  + tagmid + col + tagend + hexvalue[sztemp] + '</th>'
                        sztemp += 1
                        if sztemp%6 == 0 or sztemp==szreg:                    
                            break

                    sz = sztemp
                    grupno = grupnotemp
                    htmltext += '</tr><tr></tr>'
                htmltext += '</table></li></ul></li>'
                print(htmltext)       
            else:
                pass#htmltext += ParseTreeRegisterValue(answ, clients[id(websock)]["RequestHeads"][i], ii, i)
    msg = str(id(websock)) + ',' + 'p' + ',' + 's' + ',' + htmltext
    websock.write_message(msg)

@tornado.gen.coroutine
def SaveQueue(websock, message, ClientPool):
    status = "Template save database fail"
    try:
        l = int(len(message[4:])/2)
        future = websock.db.userdata[ClientPool[id(websock)]['name']].replace_one({'_id':message[2],'user': ClientPool[id(websock)]['name'], 'asic': message[3]}, {'_id':message[2],'user': ClientPool[id(websock)]['name'], 'asic': message[3],'queue':message[4:4+l],'tag':message[4+l:]}, True)
        result = yield future
        if result.acknowledged == True:
            status = "S"
    except Exception as error:
        print(str(error))
        status = str(error)
    if status == "S":
        msg = str(id(websock)) + ',' + 's' + ',' + 'S' + ',' + 'Saved Successfully'
    else:
        msg = str(id(websock)) + ',' + 's' + ',' + 'U' + ',' + status
    websock.write_message(msg)


@tornado.gen.coroutine
def TemplateList(websock, message, ClientPool):
    status = "Template name database fail"
    try:
        future = websock.db.userdata[ClientPool[id(websock)]['name']].find({'user':ClientPool[id(websock)]['name'], 'asic' : message[2]})
        result = yield future.to_list(length=100)
        name = []
        for i in result:
            name.append(i['_id'])
        if name != []:
            status = "S"
    except Exception as error:
        print(str(error))
        status = str(error)

    if status == "S":
        msg = str(id(websock)) + ',' + 't' + ',' + 'S' + ',' + ','.join(name)
    else:
        msg = str(id(websock)) + ',' + 't' + ',' + 'U' + ',' + status
    websock.write_message(msg)

@tornado.gen.coroutine
def LoadQueue(websock, message, ClientPool):
    status = "Queue Load database fail"
    try:
        future = websock.db.userdata[ClientPool[id(websock)]['name']].find_one({'_id':message[2], 'asic':message[3]})
        result = yield future
        if result != None:
            status = "S"
    except Exception as error:
        print(str(error))
        status = str(error)

    if status == "S":
        msg = str(id(websock)) + ',' + 'l' + ',' + 'S' + ',' + ','.join(result['queue']) + ',' + ','.join(result['tag'])
    else:
        msg = str(id(websock)) + ',' + 'l' + ',' + 'U' + ',' + status
    websock.write_message(msg)

@tornado.gen.coroutine
def DownloadTemplate(websock, message, ClientPool):
    status = 'Download Database Fail'
    try:
        future = websock.db.userdata[ClientPool[id(websock)]['name']].find_one({'_id':message[2], 'asic':message[3]})
        result = yield future
        if result != None:
            status = "S"
    except Exception as error:
        print(str(error))
        status = str(error)
    if status == "S":
        fl = open('download/' + ClientPool[id(websock)]['name']+'_'+message[3]+'_'+message[2]+'.csv', "w")
        fl.write(','.join(result['queue']) + ',' + ','.join(result['tag']))
        fl.close()
        msg = str(id(websock)) + ',' + 'd' + ',' + 'S' + ',' + ClientPool[id(websock)]['name']+'_'+message[3]+'_'+message[2]+'.csv'
    else:
        msg = str(id(websock)) + ',' + 'd' + ',' + 'U' + ',' + status
    websock.write_message(msg)


Options = { "x" : ExchangeSessionId,
            "a" : SelectAsic,
            "g" : Suggestions,
            "q" : Query,
            "s" : SaveQueue,
            "t" : TemplateList,
            "l" : LoadQueue,
            "d" : DownloadTemplate}
