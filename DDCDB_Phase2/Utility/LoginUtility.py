import tornado.gen
import smtplib
import random
import router.rns_router as router
from tornado_smtpclient.client import SMTPAsync
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText


@tornado.gen.coroutine
def Authenticate(websock, message, ClientPool):
    #print(message)
    status = "Login Problem in Server"
    try:
        future = websock.db.userdata.logins.find_one({'_id':message[2]})
        result = yield future
        print(result)
        if result == None:
            status = "Username not found"
        else:
            if result['pwd'] == message[3]:
                status = "S"
            else:
                status = "Incorrect Password"        
    except Exception as error:
        print(str(error))
        status = str(error)

    if status == "S":
        ClientPool[id(websock)] = {}
        ClientPool[id(websock)]['name'] = message[2];
        msg = str(id(websock)) + "," + "l" + ',' + 'S' 
    else:
        msg = str(id(websock)) + "," + "l" + ',' + 'U' + "," + status

    websock.write_message(msg)

@tornado.gen.coroutine
def DecoderRouterLogin(websock, message, ClientPool):
    print(message)
    status = "Login Problem in Server"
    
    ClientPool[id(websock)]['Router'] = router.Router()
    status = ClientPool[id(websock)]['Router'].Connect(message[3], message[4], message[5])
    if(status == 'Success'):
        print("[INF] [ROUT] Login Success")
        status = ClientPool[id(websock)]['Router'].BringUp()
        print(status)
        if(status == 'Success'):
            print("[INF] [ROUT] Shell bringup success")
            ClientPool[id(websock)]['Router'].Cmd("cd /harddisk")
            print(ClientPool[id(websock)]['Router'].Read())
            ClientPool[id(websock)]['Router'].Cmd("./yoda_ra_static")
            print(ClientPool[id(websock)]['Router'].Read())
        else:
            print("[INF] [ROUT] Error Shell bringup fail")
            status = "[INF] [ROUT] Error Shell bringup fail"
    else:
        print("[INF] [ROUT] Error Login")
        status = "[INF] [ROUT] Error Login"
    
    print(status)
    if status == "Success":
        #ClientPool[id(websock)]['Router'] = {}
        msg = str(id(websock)) + "," + "c" + ',' + 'S' 
    else:
        msg = str(id(websock)) + "," + "c" + ',' + 'U' + "," + status

    websock.write_message(msg)

@tornado.gen.coroutine
def DebuggerRouterLogin(websock, message, ClientPool):
    print(message)
    status = "Login Problem in Server"
    status = "D"
    if status == "D":
        ClientPool[id(websock)]['Router'] = {}
        msg = str(id(websock)) + "," + "b" + ',' + 'S' 
        websock.write_message(msg)
    else:
        msg = str(id(websock)) + "," + "b" + ',' + 'U' + "," + status
        websock.write_message(msg)

@tornado.gen.coroutine
def NewLogin(websock,message,ClientPool):
    pin=random.getrandbits(64)
    print(pin)
    s=SMTPAsync()
    yield s.connect('outbound.cisco.com',25)
    print("sucess")
    
    yield s.starttls()
    yield s.login('ntapkir','Jarvis1994_')
    me="ntapkir@cisco.com"
    you=message[2]+"@cisco.com"
    msg=MIMEMultipart('alternative')
    msg['Subject']="Account Verification"
    msg['From']=me
    msg['To']=you
    text="Hi "+message[2]+",\n\n\tVerify your email address to complete the registration of your Datapath Decoder Account.\n\tYour pin is: "+str(pin)+".\n\n\tIf you did not request for registration please ignore this mail.\n\nRegards,\nDatapath Decoders"
    part1=MIMEText(text,'plain')
    msg.attach(part1)
    yield s.sendmail(me,you,msg.as_string())
    yield s.quit()
    websock.write_message("non"+","+"p"+','+'S'+","+str(pin)+","+str(message[2])+","+str(message[3]))

@tornado.gen.coroutine
def CreateAccount(websock,message,ClientPool):

    status = "Login Database Error"
    try:
        future = websock.db.userdata.logins.insert_one({"_id":message[2], "pwd":message[3]})
        result = yield future
        status = "S"
    except Exception as error:
        print(str(error))
        status = str(error)

    if status == "S":
        msg = 'non' + "," + "a" + ',' + 'S' 
    else:
        msg = 'non' + "," + "a" + ',' + 'U' + "," + status

    websock.write_message(msg)

#    return "User Name Not Found"
#    return "Password Incorrect"

Options = { "l" : Authenticate,
            "c" : DecoderRouterLogin,
            "b" : DebuggerRouterLogin,
            "s" : NewLogin,
            "v" : CreateAccount}
