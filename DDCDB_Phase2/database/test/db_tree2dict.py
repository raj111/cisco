def node2dict(node):
    """
    Function takes class as input and converts it into dictionary. 
    Only class attributes are converted. Class methods are skipped.

    Args:
        node: Class object that needs to be converted to dictionary.

    Returns:
        dict: Returns dictionary of converted class.

    """
    attri = [i for i in node.__dict__.keys() if i[:1]!='-']
    #print(attri)
    nodedict = {}
    for i in attri:
        nodedict[i] = getattr(node, i)
    #print(nodedict)
    return dict(nodedict)

class node:
    def __init__(self):
        self.name = "raj"
        self.startaddr = "0x"
        self.description = "i am a boy"
        self.child = ["a", "b", "c"]
        self.parent = "nayan"
        self.start = ["0", "5"]
        self.access = ["R","W"]
        self.subfield_desc = ["hia", "hib"]
        self.subfield_name = ["a", "b"]
        self.end = ["5", "31"]
        self.instances = 0


if __name__ == '__main__':
    a = node()
    node2dict(a)
