import db_pymongo_test
import db_tree2dict
import parseluke
import sys

def traverse(node, googledrive):
    if node.parent != None:
        node.parent_name = node.parent.name
    else:
        node.parent_name = "None"
    node.child_name = []
    for i in node.child:
        node.child_name.append(i.name)
    d = db_tree2dict.node2dict(node)
    del d['parent']
    del d['child']
    del d['size']
    d['_id'] = d['name'].replace('.', '=')
    #print(type(d))
    #print(d['_id'])
    googledrive.SaveDocument(d)
    for i in node.child:
        traverse(i, googledrive)

a = parseluke.LukeRegisters('luke_registers_reg_list.txt')
head = a.Head
host = str(sys.argv[1])
port = int(sys.argv[2])
databasename = str(sys.argv[3])
collectionname = str(sys.argv[4])
googledrive = db_pymongo_test.Database()
if googledrive.Connect(host, port) :
    if googledrive.Connect2Database(databasename, True):
        if googledrive.Connect2Collection(collectionname, True):
            traverse(a.Head, googledrive)
