import motor.motor_tornado
from tornado.ioloop import IOLoop
from tornado import gen

@gen.coroutine
def do_insert():
    for i in range(2000):
        future = col.insert_one({'_id':'2','i': 1,"j":1})
        result = yield future
        print(result.acknowledged)

@gen.coroutine
def insert_two():
    try:
        future = col.insert_one({'_id': 1})
        result = yield future
        #print(result.acknowledged)
    except Exception as error:
        print(str(error))

    IOLoop.current().stop()

@gen.coroutine
def find_one():
    try:
        future = col.find_one({'_id': 'luke_registers'})
        result = yield future
        print(result)
    except Exception as error:
        print(str(error))

    IOLoop.current().stop()

client = motor.motor_tornado.MotorClient('localhost', 27017)
db = client.decoder
col = db.luke
IOLoop.current().run_sync(do_insert)
#IOLoop.current().add_callback(find_one)
IOLoop.current().start()

