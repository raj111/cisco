""" Module for creating Register Tree

Author  : Neha Tapkir
Email   : ntapkir@cisco.com

Description:
This module parses the text file containing register information of an ASIC.
The information includes name, address, size, subfield description etc for 
a register.All registers are maintained in a tree. Each register is linked to 
its parent register and has a list of its daughter registers
"""

#!/usr/bin/python
import re

class Node:
    def __init__(self,name,startaddr,size,parent,description,instances):
        self.name = name
        self.startaddr = startaddr
        self.size=size
        self.description = description
        self.child = []
        self.parent = parent
        self.start = []
        self.access = []
        self.subfield_desc = []
        self.subfield_name = []
        self.end = []
        self.instances = instances

def traverse(node):
    x = node
    n = []
    while x.parent!=None:
        n.append(x.name)
        x = x.parent
    n.reverse()

    for i in node.child:
        traverse(i)
    node.name = ".".join(n)
    print(".".join(n)) 


def traverse2(node, name):
    print(node.name)
    #for j in range(len(node.subfield_name)):
    #    print node.subfield_name[j]+" "+node.access[j]+" "+node.start[j]+':'+node.end[j]+" "+node.subfield_desc[j] 
    for i in node.child:
        traverse2(i, name)

def reverseLists(node):
    node.start.reverse()
    node.end.reverse()
    for i in node.child:
        reverseLists(i)

class LukeRegisters:
    def __init__(self,filename):
        self.Head = self.createTree(filename)
        self.currentPointer = self.Head
    def createTree(self,filename):
        try:
            file1 = open(filename,'r')
        except IOError:
            print("Checkfile")
        count = 0
        index=0
        first = 0
        instances=0
        first1 = 0
        flag=0
        current_level = 1
        desc=""
        for line in file1:
            if(line[0]!='#'):
                if(first1<2):
                    first1+=1
                    continue
                elif(first1==2 and flag==0):
                    words=line.split()
                    address=words[0]
                    size=words[1]
                    name=words[2].split('#')[0]
                    flag=1
                    head=Node(name,size,address,None,"nodescription",0)
                    currentnode=head
                    continue
                if(line[0]=='0'):
                    instances=0;
                    words=line.split()
                    address = words[0]
                    size = words[1]
                    description=" ".join(words[3:])
                    i=0
                    while(words[2][i]=='+'):
                        i=i+1
                    if("#" in words[2]):
                        name = words[2].split('#')[0]
                        name=name.strip('+')
                    elif("/" in words[2]):
                        name = words[2].split('/')[0]
                        name=name.strip('+')
                    if("[" in name):
                        instances=name.split('[')[1]
                        name=name.split('[')[0]
                        instances=instances.split('*')[0]
                    #if(i==1):
                        #str1="."+name
                    #    name=str1
                    #elif(i==2):
                    #    addrmap = str1.split('.')[1]
                        #str1="."+addrmap+"."+name
                    #    name=str1
                    #else:
                    #    addrmap    = str1.split('.')[1]
                     #   regfile = str1.split('.')[2]
                        #str1="."+addrmap+"."+regfile+"."+name
                     #   name=str1
                    if(i==current_level):
                        temp = Node(name,address,size,currentnode,description,instances)
                        currentnode.child.append(temp)
                    elif(i>current_level):
                        current_level = current_level+1 
                        currentnode = currentnode.child[-1]
                        temp = Node(name,address,size,currentnode,description,instances)
                        currentnode.child.append(temp)
                    else:
                        while(current_level!=i):
                            current_level = current_level-1
                            currentnode = currentnode.parent
                        temp = Node(name,address,size,currentnode,description,instances)
                        currentnode.child.append(temp)
                else:
                    words = line.split()
                    if(words[0]!="ALIAS:"):
                        currentnode.child[-1].subfield_name.append(words[0])
                        currentnode.child[-1].access.append(words[1])
                        length = words[2].split(':')
                        currentnode.child[-1].start.append(length[0])
                        currentnode.child[-1].end.append(length[1])
                        if(len(words)>3):
                            currentnode.child[-1].subfield_desc.append(" ".join(words[3:]))
                        else:
                            currentnode.child[-1].subfield_desc.append("no description")
        file1.close()
        traverse(head)
        #traverse2(head, "")
        return head

if __name__ == "__main__":
    head = LukeRegisters("luke_registers_reg_list.txt")
