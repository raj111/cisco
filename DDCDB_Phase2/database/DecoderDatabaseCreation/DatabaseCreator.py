import MongoDBWrapper
import Class2DictConverter
import TreeGenerator
import sys

def Save2Database(node, googledrive):
    d = Class2DictConverter.node2dict(node)
    d['_id'] = d['_id'].replace('.', '=')
    del d['parent']
    del d['child']
    del d['size']
    
    googledrive.SaveDocument(d)
    for i in node.child:
        Save2Database(i, googledrive)

a = TreeGenerator.LukeRegisters(sys.argv[1])
host = str(sys.argv[2])
port = int(sys.argv[3])
databasename = str(sys.argv[4])
collectionname = str(sys.argv[5])

googledrive = MongoDBWrapper.Database()
if googledrive.Connect(host, port) :
    if googledrive.Connect2Database(databasename, True):
        if googledrive.Connect2Collection(collectionname, True):
            Save2Database(a.Head, googledrive)
