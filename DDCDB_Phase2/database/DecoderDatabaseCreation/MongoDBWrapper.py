import sys
from pymongo import MongoClient

class Database:
    def __init__(self):
        """
            Blank constructor.
        """
        pass

    def Connect(self, hostname, portnumber):
        """
        Connect to MongoDB database.

        Args:
            hostname (str): MongoDB server ip address.
            portnumber (int): MongoDB server port number.

        Returns:
            NONE.
        """
        try:
            self.Connection = MongoClient(hostname, portnumber)
            return True
        except Exception as error:
            print('[ERR] [DBase] [ConnectModule] ' + str(error) + '\n')
            return False

    def Connect2Database(self, databasename, create = False):
        """
        Connect to MongoDB database.

        Args:
            hostname (str): MongoDB server ip address.
            portnumber (int): MongoDB server port number.

        Returns:
            NONE.
        """
        if databasename in self.Connection.database_names():
            try:
                self.Database = self.Connection[databasename]
                return True
            except Exception as error:
                print('[ERR] [DBase] [OpenDatabase] ' + str(error) + '\n')
                return False
        else:
            if create :
                try:
                    self.Database = self.Connection[databasename]
                    return True
                except Exception as error:
                    print('[ERR] [DBase] [CreateDatabase] ' + str(error) + '\n')
                    return False
            else:
                return True

    def Connect2Collection(self, collectionname, create = False):
        """
        Connect to MongoDB database.

        Args:
            hostname (str): MongoDB server ip address.
            portnumber (int): MongoDB server port number.

        Returns:
            NONE.
        """
        if collectionname in self.Database.collection_names():
            try:
                self.Collection = self.Database[collectionname]
                return True
            except Exception as error:
                print('[ERR] [DBase] [OpenCollection] ' + str(error) + '\n')
                return False
        else:
            if create:
                try:
                    self.Collection = self.Database[collectionname]
                    return True
                except Exception as error:
                    print('[ERR] [DBase] [CreateCollection] ' + str(error) + '\n')
                    return False
            else:
                return True

    def SaveDocument(self, document, update = False):
        """
        Connect to MongoDB database.

        Args:
            hostname (str): MongoDB server ip address.
            portnumber (int): MongoDB server port number.

        Returns:
            NONE.
        """
        documenttemp = dict(document)
        #print(documenttemp)
        try:
            self.Collection.insert_one(documenttemp)
            return True
        except Exception as error:
            print('[ERR] [DBase] [SaveDoc] ' + str(error) + '\n')
            return False

if __name__ == '__main__':
    host = str(sys.argv[1])
    port = int(sys.argv[2])
    databasename = str(sys.argv[3])
    collectionname = str(sys.argv[4])
    googledrive = Database()
    if googledrive.Connect(host, port) :
        if googledrive.Connect2Database(databasename, True):
            if googledrive.Connect2Collection(collectionname, True):
                a = {"name":"raj","surname":"samant"}
                googledrive.SaveDocument(a)
                print(a)
