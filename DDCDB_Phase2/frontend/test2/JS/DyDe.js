var Websocket;
var session_id;
var current_tree_lvl = 1;
var curr_reg_name = [];
var que_reg_name = [];
var multiple_instance = 0;

function onBodyLoad()
{

}

function SelectAsic()
{
    curr_reg_name = [];
    que_reg_name = [];
    current_tree_lvl = 1;
    document.getElementById('treeregister').innerHTML = "";
    document.getElementById("queueregister").innerHTML = "";
    Websocket.send(session_id + ',' + 'a' + ',' + document.getElementById('mySelect').value + '_registers');
}

function recieveMsg(msg)
{
    msg = msg.split(",");
    console.log(msg);

    switch(msg[1]) {
        case "c":
            if(msg[2] == "U") {
                document.getElementById('decwarning').innerHTML = "Cannot find asics from DB";
    	        document.getElementById("decwarning").style.display = "block";
                setTimeout(function (){document.getElementById("decwarning").style.display = "none"}, 4000);
            } else {
                var selectelem = document.getElementById('mySelect');
                for(var i=3; i<msg.length; i++) {
                    var optionelem = document.createElement('option');
            		optionelem.text = optionelem.value = msg[i];
        		    selectelem.add(optionelem, 0);
                }
                document.getElementById("mySelect").selectedIndex = -1;
            }
        break;
        case "g":
            if(msg[2] == "U") {
                document.getElementById('decwarning').innerHTML = "Cannot get child names suggestions";
    	        document.getElementById("decwarning").style.display = "block";
                setTimeout(function (){document.getElementById("decwarning").style.display = "none"}, 4000);
            } else {
                if (msg[4] == '0' || multiple_instance != 0) {
                    me._list = msg.slice(5, msg.length);
                    if (me._list.length == 1 && me._list[0] == "") {
                        document.getElementById('awesompleteinputtext').disabled = true;
                    } else {
                        document.getElementById('awesompleteinputtext').disabled = false;
                    }
                    var newdiv = document.createElement('texttree');
                    newdiv.id = 'cl-' + String(current_tree_lvl);
                    if (multiple_instance != 0) {
                        msg[3] = msg[3] + '[' + String(multiple_instance) + ']';
                        multiple_instance = 0;
                        document.getElementById("decwarning").style.display = "none";
                    }
                    newdiv.innerHTML = "&nbsp;".repeat(4*current_tree_lvl) + "|-" + msg[3] + '<br>';
                    curr_reg_name.push(msg[3]);
                    newdiv.addEventListener("click", function(){rollbackcurrreg(this);});
                    document.getElementById("treeregister").appendChild(newdiv);
                    document.getElementById('inputtextbox').style.display = 'block';
                    document.getElementById('content2').style.display = 'block';
                    current_tree_lvl = current_tree_lvl + 1;
                    document.getElementById('awesompleteinputtext').value = "";
                } else {
                    multiple_instance = 1;
                    document.getElementById('decwarning').innerHTML = "Multiple Instance Register, Please choose instance between 0 - " + String(parseInt(msg[4])-1);
	                document.getElementById("decwarning").style.display = "block";
                    document.getElementById('awesompleteinputtext').value = document.getElementById('awesompleteinputtext').value.replace('.', '=');
                }
            }
        break;
        case "s":
            document.getElementById("popupwarning").innerHTML = msg[3];
	        document.getElementById("popupwarning").style.display = "block";
            setTimeout(function(){document.getElementById("popupwarning").style.display = "none"}, 4000);
        break;
        case "t":
            if(msg[2] == "U") {
                document.getElementById("popupwarning").innerHTML = msg[3];
	            document.getElementById("popupwarning").style.display = "block";
                setTimeout(function(){document.getElementById("popupwarning").style.display = "none"}, 4000);  
            } else {
                var selectelem = document.getElementById('templateSelect');
                for(var i=3; i<msg.length; i++) {
                    var optionelem = document.createElement('option');
            		optionelem.text = optionelem.value = msg[i];
        		    selectelem.add(optionelem, 0);
                }
                document.getElementById("templateSelect").selectedIndex = -1;
            }
        break;
        case "l":
            if(msg[2] == "U") {
                document.getElementById("popupwarning").innerHTML = msg[3];
	            document.getElementById("popupwarning").style.display = "block";
                setTimeout(function(){document.getElementById("popupwarning").style.display = "none"}, 4000);  
            } else {
                for(var i=0; i<(msg.length-3)/2; i++) {
                    que_reg_name.push(msg[i+3]);
                    var newdiv = document.createElement('quelist');
                    newdiv.id = msg[i+3];
                    newdiv.addEventListener("click", function(){deletequeuenodes(this);});
                    if(msg[i+3+(msg.length-3)/2] == "leaf") {
                        newdiv.innerHTML = '<span style="color: blue">[LEAF]</span> ' + msg[i+3].replace(/=/g, '.') + '<br>';
                    } else {
                        newdiv.innerHTML = '<span style="color: green">[TREE]</span> ' + msg[i+3].replace(/=/g, '.') + '<br>';
                    }
                    document.getElementById("queueregister").insertBefore(newdiv, document.getElementById("queueregister").childNodes[0]);      
                }
            }
        break;
        case "d":
            if(msg[2] == "U") {
                document.getElementById("popupwarning").innerHTML = msg[3];
	            document.getElementById("popupwarning").style.display = "block";
                setTimeout(function(){document.getElementById("popupwarning").style.display = "none"}, 4000);  
            } else {
                window.open("/DC/DWN/file/"+msg[3]);
            }
        break;
        case "p":
            if(msg[2] == "U") {
                document.getElementById("popupwarning").innerHTML = msg[3];
	            document.getElementById("popupwarning").style.display = "block";
                setTimeout(function(){document.getElementById("popupwarning").style.display = "none"}, 4000);  
            } else {
                console.log(msg[3]);
                document.getElementById("treeid").innerHTML = msg[3];
                document.getElementById("resultmodel").style.display = "block";
            }
        break;
    }
}

function rollbackcurrreg(elem)
{
    var elemid = elem.id.split('-')[0];
    var elemnum = parseInt(elem.id.split('-')[1]);
    for(var i=elemnum; i<current_tree_lvl; i++) {
        document.getElementById(elemid + '-' + String(i)).parentNode.removeChild(document.getElementById(elemid + '-' + String(i)));
    }
    current_tree_lvl = elemnum;
    curr_reg_name = curr_reg_name.slice(0, elemnum);    
    Websocket.send(session_id + ',' + 'g' + ',' + curr_reg_name.join('='));
    curr_reg_name.pop();
}

function deletequeuenodes(elem)
{
    if(que_reg_name.indexOf(elem.id) != -1) {
        que_reg_name.splice(que_reg_name.indexOf(elem.id), 1);
    }
    elem.parentNode.removeChild(elem);
}

function AddRegisterQueue()
{
    console.log(curr_reg_name);
    console.log(que_reg_name);
    que_reg_name.push(curr_reg_name.join('='));
    var newdiv = document.createElement('quelist');
    newdiv.id = curr_reg_name.join('=');
    newdiv.addEventListener("click", function(){deletequeuenodes(this);});

    if(me._list.length == 1 && me._list[0] == "") {
        newdiv.innerHTML = '<span style="color: blue">[LEAF]</span> ' + curr_reg_name.join('.') + '<br>';
    } else {
        newdiv.innerHTML = '<span style="color: green">[TREE]</span> ' + curr_reg_name.join('.') + '<br>';
    }
    document.getElementById("queueregister").insertBefore(newdiv, document.getElementById("queueregister").childNodes[0]);
    rollbackcurrreg(document.getElementById('cl-1'));
}

function SubmitRegisterQueue()
{
    if (que_reg_name.length != 0) {
        var tag = [];
        for (var i=0; i<que_reg_name.length; i++) {
            if (document.getElementById(que_reg_name[i]).innerHTML.search("TREE") == -1) {
                tag.push('l');
            } else {
                tag.push('t');
            }
        }
        Websocket.send(session_id + "," + "q" + "," + que_reg_name.join(',') + ',' + tag.join(','));
    } else {
        document.getElementById("popupwarning").innerHTML = "Your Queue is Empty";
	    document.getElementById("popupwarning").style.display = "block";
        setTimeout(function(){document.getElementById("popupwarning").style.display = "none"}, 4000);
    }
}

function savetemplate(templatename)
{   
    if (que_reg_name.length != 0) {
        var tag = [];
        for (var i=0; i<que_reg_name.length; i++) {
            if (document.getElementById(que_reg_name[i]).innerHTML.search("TREE") == -1) {
                tag.push('leaf');
            } else {
                tag.push('tree');
            }
        }
        Websocket.send(session_id + "," + "s" + "," + templatename + "," + document.getElementById('mySelect').value + "," + que_reg_name.join(',') + ',' + tag.join(','));
    } else {
        document.getElementById("popupwarning").innerHTML = "Your Queue is Empty";
	    document.getElementById("popupwarning").style.display = "block";
        setTimeout(function(){document.getElementById("popupwarning").style.display = "none"}, 4000);
    }
}

function loadtemplate(del)
{
    if(del) {
        var myNode = document.getElementById("queueregister");
        while (myNode.firstChild) {
            myNode.removeChild(myNode.firstChild);
        }
        que_reg_name = [];
    }
    Websocket.send(session_id + ',' + 'l' + ',' + document.getElementById('templateSelect').value + ',' + document.getElementById('mySelect').value);
}

function ManageRegisterQueue()
{
    Websocket.send(session_id + ',' + 't' + ',' + document.getElementById('mySelect').value);
    document.getElementById('templateModal').style.display = "block";
}

function closepopup(elem)
{
    elem.parentNode.parentNode.style.display = "none";
    if(elem.parentNode.parentNode.id == 'templateModal') {
        var sel = document.getElementById('templateSelect');
        for(i = sel.options.length - 1; i >= 0; i--) {
            sel.remove(i);
        }
    }
}

function downloadtemplate()
{
    Websocket.send(session_id + ',' + 'd' + ',' + document.getElementById('templateSelect').value + ',' + document.getElementById('mySelect').value);
}

function uploadtemplate(del)
{
    if(del) {
        var myNode = document.getElementById("queueregister");
        while (myNode.firstChild) {
            myNode.removeChild(myNode.firstChild);
        }
        que_reg_name = [];
    }
	var input = document.getElementById('fileInput');
	var reader = new FileReader();
	reader.onload = function(){
		iptxt = reader.result;
		iptxt = iptxt.replace(/(\r\n|\n|\r)/gm,"");
        msg = session_id + ',' + 'l' + ',' + 'S' + ',' + iptxt;
        recieveMsg(msg);
	};
	reader.readAsText(input.files[0]);
}
