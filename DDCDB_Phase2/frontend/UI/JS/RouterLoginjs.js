var Websocket;
var session_id;
var pin;

function onLoad()
{
	var url = document.URL.replace("http", "ws");
	url = url.replace(url.substring(url.indexOf('/', 5), url.length), "/") + "LogInWebSocket";
	console.log(url);
	Websocket = new WebSocket(url);
	console.log("[INF] [SOCK] Socket Connection Created");
	Websocket.onmessage = function (e){recieveMsg(e.data);}	
}

function recieveMsg(msg)
{
    msg = msg.split(",");
    console.log(msg);
    if(msg[2] == 'S') {
        switch(msg[1]) {
            case "l":
            	session_id = msg[0];
                document.getElementById('logincontent').style.display = 'none';
                document.getElementById('selectoption').style.display = 'block';
            break;
            case "c":
                window.location="DC/" + session_id;
            break;
            case "b":
                window.location="DB/" + session_id;
            break;
            case "p":
            	pin=msg[3];
                console.log(msg[3])
                document.getElementById('pinmng').style.display = 'block';
            break;
            case "a":
                document.getElementById('pinmng').style.display = 'none'; 
                document.getElementById("contentwarning").innerHTML = 'New User Created';
        	    document.getElementById("contentwarning").style.display = "block";
                setTimeout(HideError, 4000);
            break;
        }
    } else {
        document.getElementById("contentwarning").innerHTML = msg[3];
	    document.getElementById("contentwarning").style.display = "block";
        setTimeout(HideError, 4000);
    }
}

function OnLoginSubmit() 
{
    var msg = document.getElementById("lnunfld").value + "," + "l" + "," + document.getElementById("lnunfld").value + "," + document.getElementById("lnpwfld").value;
    Websocket.send(msg);
}

function OnDecoderSubmit() 
{
	var formdata = [session_id, 'c'];
	var formvalidity = true;
    var formip = document.getElementById("decodecontent").getElementsByTagName("input");
    var formtxt = document.getElementById("decodecontent").getElementsByTagName("text");
    
    for(var i=0; i<formip.length; i++) {
        if(formip[i].value == "") {
            formvalidity = false;
            formtxt[i].style.color = "red";       
            break;
        } else {
            formvalidity = true;
            formdata.push(formip[i].value);
        }
    }
    
    if(formvalidity) {
        Websocket.send(formdata.join(','));
    } else {
        document.getElementById("contentwarning").innerHTML = "Invalid Input";
	    document.getElementById("contentwarning").style.display = "block";
        setTimeout(HideError, 4000); 
    }
}

function OnDebuggerSubmit() 
{
	var formdata = [session_id, 'b'];
	var formvalidity = true;
    var formip = document.getElementById("debuggercontent").getElementsByTagName("input");
    var formtxt = document.getElementById("debuggercontent").getElementsByTagName("text");
    
    for(var i=3; i<formip.length; i++) {
        if(formip[i].value == "") {
            formvalidity = false;
            formtxt[i].style.color = "red";       
            break;
        } else {
            formvalidity = true;
            formdata.push(formip[i].value);
        }
    }
    
    if(formvalidity) {
        Websocket.send(formdata.join(','));
    } else {
        document.getElementById("contentwarning").innerHTML = "Invalid Input";
	    document.getElementById("contentwarning").style.display = "block";
        setTimeout(HideError, 4000); 
    }
}


function DisplayModule(name) 
{
    if(name == 'dec') {
    	document.getElementById('decodecontent').style.display = 'block';
    	document.getElementById('debuggercontent').style.display = 'none';
    	module = 0;        
    } else {
        document.getElementById('decodecontent').style.display = 'none';
	    document.getElementById('debuggercontent').style.display = 'block';
	    module = 1;
    }
}

function HideError() 
{
	document.getElementById("contentwarning").style.display = "none";
    var formtxt = document.getElementById("decodecontent").getElementsByTagName("text");
    for(var i=0; i<formtxt.length; i++)
        formtxt[i].style.color = "black";
    formtxt = document.getElementById("debuggercontent").getElementsByTagName("text");
    for(var i=0; i<formtxt.length; i++)
        formtxt[i].style.color = "black";
}


function OnSignUp()
{
	document.getElementById("contentwarning").innerHTML="Please verify your username by entering the pin code sent to your email";	
	document.getElementById("contentwarning").style.display="block";
    setTimeout(HideError, 10000);
	Websocket.send("non" + "," + "s" + "," + document.getElementById("lnunfld").value + "," + document.getElementById("lnpwfld").value);
}

function OnPin()
{
	if(document.getElementById("lnpnfld").value==pin) {
		Websocket.send("none" + "," + "v" + "," + document.getElementById("lnunfld").value + "," + document.getElementById("lnpwfld").value);
    } else {
	    document.getElementById("contentwarning").innerHTML="Pin did not match";	
	    document.getElementById("contentwarning").style.display="block";
        setTimeout(HideError, 4000);
	}	

}
