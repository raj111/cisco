var sys = require("sys");
var my_http = require("http");
var path = require("path");
var url = require("url");
var filesys = require("fs");
var io = require("/usr/local/lib/node_modules/socket.io");
var telnet = require("/usr/local/lib/node_modules/telnet-client");

var server = my_http.createServer(my_server);
var connection = new telnet();
 
var params = {
  host: '127.0.0.1',
  port: 8000,
  shellPrompt: '/ # ',
  timeout: 1500,
  // removeEcho: 4 
};


function my_server(request,response)
{
	var my_path = url.parse(request.url).pathname;
	var full_path = path.join(process.cwd(),my_path);
	filesys.exists(full_path,function(exists){
    if(!exists){
      response.writeHeader(404, {"Content-Type": "text/plain"});  
      response.write("404 Notdgdg Found\n");  
      response.end();
    }
    else{
      filesys.readFile(full_path, "binary", function(err, file) {  
           if(err) {  
               response.writeHeader(500, {"Content-Type": "text/plain"});  
               response.write(err + "\n");  
               response.end();  
          
           }  
         else{
          response.writeHeader(200);  
              response.write(file, "binary");  
              response.end();
        }
            
      });
    }
  });
}

connection.on('ready', function(prompt) {
  connection.exec("open", function(err, response) {
    console.log(response);
  });
});
 
connection.on('timeout', function() {
  console.log('socket timeout!')
  connection.end();
});
 
connection.on('close', function() {
  console.log('connection closed');
});
 
connection.connect(params);

server.listen(8080);
sys.puts("Server Running on 8080");
io.listen(server);     

var listener = io.listen(server);
listener.sockets.on('connection', my_onconnection);

function my_onconnection(socket)
{
	socket.emit('message', {'message': 'hello world'});
	socket.on('client_data', function(data){
		var replyfromtelenet;
		if(data.letter == "RegisterA"){
			replyfromtelenet = "0x1111";
		}
		else if(data.letter == "RegisterB"){
			replyfromtelenet = "0x2222";
		}
		else if(data.letter == "exit"){
			replyfromtelenet = "Closing telenet connection";
		}	
		else{
			replyfromtelenet = "unknown register";
		}
		socket.emit('message', {'message': replyfromtelenet});
		//process.stdout.write(data.letter);
	});
}


