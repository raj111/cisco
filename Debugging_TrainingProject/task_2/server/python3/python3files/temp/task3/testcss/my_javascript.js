//Global coordinate variable
// elemid : [gx, gy, rx, ry, sx, sy]
var ElemCoordi = {};
var coordholder = {};

window.onload = function () 
{
	var width = window.innerWidth || document.documentElement.clientWidth || document.body.clientWidth;
	var height = window.innerHeight || document.documentElement.clientHeight || document.body.clientHeight;

	var shapes;
	var connections = [];
	var numbnodes = 4;
	var nodenames = ["a.png","b.png","c.png","d.png"];
	var coordi = [[50,50], [300,50], [300, 300], [50, 300]];
	var admat = [[0, 1, 1, 1],
				[1, 0, 0, 1],
				[1, 0, 0, 1],
				[1, 0, 1, 0]];
	var dragger = function ()
	{
    	this.ox = (this.type == "rect" || this.type == "image") ? this.attr("x") : this.attr("cx");
    	this.oy = (this.type == "rect" || this.type == "image") ? this.attr("y") : this.attr("cy");
		this.animate(200);
	}

	var move = function (dx, dy) 
	{
		var att = (this.type == "rect" || this.type == "image") ? {x: this.ox + dx, y: this.oy + dy} : {cx: this.ox + dx, cy: this.oy + dy};
        this.attr(att);
        for (var i = 0; i < connections.length; i++) 
		{
            conncreatefunc(r, connections[i]);
        }
    }
	
	var up = function () 
	{
		//this.animate({"fill-opacity": 0}, 500);
		ElemCoordi[this.node.raphaelid][2] = this.ox;
		ElemCoordi[this.node.raphaelid][3] = this.oy;
		ElemCoordi[this.node.raphaelid][0] = coordholder.left + this.ox;
		ElemCoordi[this.node.raphaelid][1] = coordholder.top + this.oy;
		//console.log(ElemCoordi);
		this.animate(200);
    }

	function onblockclickfunc(thisid, event)
	{
		//console.log(thisid.raphaelid + " - " + event.button);
		
	}


	


	function onblockdblclickfunc(thisid, event)
	{
		//console.log(thisid.raphaelid);
		createbox(thisid);		
	}

	var conncreatefunc = function (r, obj1, obj2, line) 
	{

		if (obj1.line && obj1.from && obj1.to) {
			line = obj1;
			obj1 = line.from;
			obj2 = line.to;
    	}
    
		var bb1 = obj1.getBBox();
    	var bb2 = obj2.getBBox();
    	var p = [	{x: bb1.x + bb1.width / 2, y: bb1.y - 1},
		    		{x: bb1.x + bb1.width / 2, y: bb1.y + bb1.height + 1},
		    		{x: bb1.x - 1, y: bb1.y + bb1.height / 2},
		    		{x: bb1.x + bb1.width + 1, y: bb1.y + bb1.height / 2},
		    		{x: bb2.x + bb2.width / 2, y: bb2.y - 1},
		   	 		{x: bb2.x + bb2.width / 2, y: bb2.y + bb2.height + 1},
		    		{x: bb2.x - 1, y: bb2.y + bb2.height / 2},
		    		{x: bb2.x + bb2.width + 1, y: bb2.y + bb2.height / 2}
				];

    	var d = {};
		var dis = [];

    	for (var i = 0; i < 4; i++) {
        	for (var j = 4; j < 8; j++) {
            	var dx = Math.abs(p[i].x - p[j].x);
            	var dy = Math.abs(p[i].y - p[j].y);
            	if ((i == j - 4) || (((i != 3 && j != 6) || p[i].x < p[j].x) && ((i != 2 && j != 7) || p[i].x > p[j].x) && ((i != 0 && j != 5) || p[i].y > p[j].y) && ((i != 1 && j != 4) || p[i].y < p[j].y))) {
                	dis.push(dx + dy);
                	d[dis[dis.length - 1]] = [i, j];
            	}
        	}
    	}

    	if (dis.length == 0) {
        	var res = [0, 4];
    	}	 
		else {
        	res = d[Math.min.apply(Math, dis)];
    	}

    	var x1 = p[res[0]].x;
    	var y1 = p[res[0]].y;
    	var x4 = p[res[1]].x;
    	var y4 = p[res[1]].y;

    	dx = Math.max(Math.abs(x1 - x4) / 2, 10);
    	dy = Math.max(Math.abs(y1 - y4) / 2, 10);

    	var x2 = [x1, x1, x1 - dx, x1 + dx][res[0]].toFixed(3);
    	var y2 = [y1 - dy, y1 + dy, y1, y1][res[0]].toFixed(3);
    	var x3 = [0, 0, 0, 0, x4, x4, x4 - dx, x4 + dx][res[1]].toFixed(3);
    	var y3 = [0, 0, 0, 0, y1 + dy, y1 - dy, y4, y4][res[1]].toFixed(3);

    	var path = ["M", x1.toFixed(3), y1.toFixed(3), "C", x2, y2, x3, y3, x4.toFixed(3), y4.toFixed(3)].join(",");

    	if (line && line.line) {
        	line.bg && line.bg.attr({path: path});
        	line.line.attr({path: path});
    	}
		else{
        	var color = typeof line == "string" ? line : "#000";
        	return{
            	//bg: bg && bg.split && this.path(path).attr({stroke: bg.split("|")[0], fill: "none", "stroke-width": bg.split("|")[10] || 10}),
            //line: this.path(path).attr({stroke: color, fill: "none"}),
				line: r.path(path).attr({stroke: color, "stroke-width":5}),
            	from: obj1,
            	to: obj2
        	};
    	}
	};


	r = Raphael("holder", width, height);
    //connections = [],
	var shapes = [];
	var color;
	for (var i = 0; i < numbnodes; i++) {
		shapes[i] = r.ellipse(coordi[i][0], coordi[i][1], 30, 20);
        color = Raphael.getColor();
        shapes[i].attr({fill: color, stroke: color, "fill-opacity": 1, "stroke-width": 2, cursor: "move"});
		shapes[i].drag(move, dragger, up);
		shapes[i].node.onclick = function (event){onblockclickfunc(this, event);}
		shapes[i].node.ondblclick = function (event){onblockdblclickfunc(this, event);}
		
		ElemCoordi[String(shapes[i].node.raphaelid)] = [coordholder.left + coordi[i][0], 
														coordholder.top + coordi[i][1],
														coordi[i][0],
														coordi[i][0],
														30, 
														20 ];
    }

	//console.log(ElemCoordi);
	connections.push(conncreatefunc(r, shapes[0], shapes[1], "#00f", "#00f|1"));
    connections.push(conncreatefunc(r, shapes[1], shapes[2], "#0f0", "#0f0|1"));
    connections.push(conncreatefunc(r, shapes[0], shapes[2], "#0f0", "#0f0|1"));
    connections.push(conncreatefunc(r, shapes[1], shapes[3], "#f00", "#f00|1"));
	coordholder = GetAbsoluteOffset(document.getElementById("holder"));
};



