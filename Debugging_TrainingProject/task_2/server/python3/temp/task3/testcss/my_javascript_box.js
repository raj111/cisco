function createbox(thisid)
{
	var divn = document.createElement("div");
	//console.log(thisid);
	divn.innerHTML = "hi";
	divn.setAttribute("class", "elemclass");
	divn.setAttribute("id", thisid.raphaelid + "elem");
	divn.setAttribute("onmousedown", 'mousestartmoving(this, "body", event)');
	divn.setAttribute("onmouseup", 'mousestopmoving(this, "body")');
	divn.setAttribute("ondblClick", 'closebox(this)');

	var b = GetAbsoluteOffset(document.getElementById("holder"));
	
	//console.log(thisid.getBBox().y + " - " + thisid.getBBox().x + " | " +thisid.getBBox().height/2 + " - " + thisid.getBBox().width/2);
	divn.style.top = (b.top + thisid.getBBox().y +  thisid.getBBox().height/2 + 50) + "px";
	divn.style.left = (b.left + thisid.getBBox().x + thisid.getBBox().width/2 + 50) +  "px";
	divn.style.width = "50px";
	divn.style.height = "50px";
	divn.style.position = "absolute";

	createline( (b.left + thisid.getBBox().x + thisid.getBBox().width/2 + 0),
				(b.top + thisid.getBBox().y +  thisid.getBBox().height/2 + 0), 
				(b.left + thisid.getBBox().x + thisid.getBBox().width/2 + 50), 
				(b.top + thisid.getBBox().y +  thisid.getBBox().height/2 + 50), 
				divn.getAttribute("id"));
	document.body.appendChild(divn);
	//console.log(thisid);
	ElemCoordi[thisid.raphaelid + "elem"] =[b.left + thisid.getBBox().x + thisid.getBBox().width/2,
											b.left + thisid.getBBox().y + thisid.getBBox().height/2,
											0,
											0,
											50,
											50];
}		

function mousemoveelem(divid, aX, aY)
{
    divid.style.left = aX + 'px';
    divid.style.top = aY + 'px';
}


			function mousemoveemptyelem()
			{
			}
				
			function mousestopmoveelem(divid, container)
			{
				//console.log("hi");
            	//var a = document.createElement('script');
				document.getElementById(container).style.cursor='default';
                document.onmousemove = mousemoveemptyelem;
				
            }

            function mydragg()
			{
				//console.log("hi");
			}

	 		function mousestartmoving(divid, container, evt)
			{

				//console.log("inside down");
	            evt = evt || window.event;
	            var posX = evt.clientX;
                var posY = evt.clientY;
	            var divTop = divid.style.top;
                var divLeft = divid.style.left;
				divTop = divTop.replace('px','');
                divLeft = divLeft.replace('px','');
                var diffX = posX - divLeft;
                var diffY = posY - divTop;
				
				var eWi = parseInt(divid.style.width);
				var eHe = parseInt(divid.style.height);
				//console.log("st = " + String(posX) + " " + String(posY) + " - " + String(divLeft) + " " + String(divTop) + " | " + String(parseInt(divTop)+2));
document.getElementById(divid.id + "line").parentNode.removeChild(document.getElementById(divid.id + "line"));
				if((eWi - diffX  < -20) && (eHe - diffY < -20))
				{
					//console.log("hi");
					//break;
				}
				else
				{

					var cWi = parseInt(document.getElementById(container).style.width);
					var cHe = parseInt(document.getElementById(container).style.height);
                
	
				//console.log("st = " + String(diffX) + " " + String(diffY));
				//console.log(this.id);
				
					
					document.getElementById(container).style.cursor='move';
                	document.onmousemove = function(evt){
                        evt = evt || window.event;
                        var posX = evt.clientX;
                        var posY = evt.clientY;
						
                        var aX = posX - diffX;
                        var aY = posY - diffY;
						if (aX < 0) aX = 0;
						if (aY < 0) aY = 0;
						//if (aX + eWi > cWi) aX = cWi - eWi;
						//if (aY + eHe > cHe) aY = cHe -eHe;
	                	mousemoveelem(divid,aX,aY);
                	}
                }    
    		}

          	function mousestopmoving(thisid, container)
			{				
	
	            //var a = document.createElement('script');
				//console.log(thisid);
				ElemCoordi[thisid.id] =[parseInt(thisid.style.left.replace('px','')),
										parseInt(thisid.style.top.replace('px','')),
										0,
										0,
										parseInt(thisid.style.width.replace('px','')),
										parseInt(thisid.style.height.replace('px',''))];
				

				//console.log(thisid.id);
				var idmodel = thisid.id.replace('elem', '');
				createline( (ElemCoordi[idmodel][0]),
							(ElemCoordi[idmodel][1]), 
							(ElemCoordi[idmodel+"elem"][0]), 
							(ElemCoordi[idmodel+"elem"][1]),
							thisid.id);
				document.getElementById(container).style.cursor='default';
                document.onmousemove = mousemoveemptyelem;
				//console.log(thisid.id + "---");
				//console.log(ElemCoordi);
            }

	function closebox(thisid)
	{
    	//console.log(document.getElementById(thisid.id).style.width);
		//console.log(document.getElementById(thisid.id));
		//console.log( "delete this element");
		document.getElementById(thisid.id).parentNode.removeChild(document.getElementById(thisid.id));
		document.getElementById(thisid.id + "line").parentNode.removeChild(document.getElementById(thisid.id + "line"));
		delete ElemCoordi[thisid.id];
		//console.log(ElemCoordi);
	}


