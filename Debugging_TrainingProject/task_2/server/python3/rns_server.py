import re
import os

# Webserver Modules
import tornado.httpserver
import tornado.ioloop
import tornado.options
import tornado.web
import tornado.websocket
import time

# Modules
import rns_database
import rns_router
import rns_decoder

pyc = []
clients = {} # Client list
Router = []

def init(websock, msg):
    print clients[id(websock)]["head"].name
    msg = ["s"] + clients[id(websock)]["head"].childnames
    msg = ",".join(msg)
    websock.write_message(msg)

def suggst(websock, mssg):
    indx = clients[id(websock)]["head"].childnames.index(mssg[1])
    clients[id(websock)]["head"] = clients[id(websock)]["head"].child[indx]
    msg = ["s"] + clients[id(websock)]["head"].childnames
    msg = ",".join(msg)
    websock.write_message(msg)

def goup(websock, mssg):
    step = int(mssg[1])
    for i in range(step):
        clients[id(websock)]["head"] = clients[id(websock)]["head"].parent
    msg = ["s"] + clients[id(websock)]["head"].childnames
    msg = ",".join(msg)
    websock.write_message(msg)

def add(websock, mssg):
    mssg.pop(0)
    clients[id(websock)]["RegisterQuery"].append(mssg)
    clients[id(websock)]["RequestHeads"].append(clients[id(websock)]["head"])
    if clients[id(websock)]["head"].childnames == []:
        mssg = ["a"] + ["l"] + mssg
        clients[id(websock)]["RegisterType"].append("l")
    else:
        mssg = ["a"] + ["t"] + mssg
        clients[id(websock)]["RegisterType"].append("t")
    mssg = ",".join(mssg)
    websock.write_message(mssg)

def remv(websock, mssg):
    clients[id(websock)]["RegisterQuery"][int(mssg[1])] = []
    clients[id(websock)]["RequestHeads"][int(mssg[1])] = []
    clients[id(websock)]["RegisterType"] = []

def exitrouter(websock, mssg):
    Router.Close()

def ParseSingleRegisterValue(comnddump, node, name, i):
    hexvalue = ""
    for j in comnddump:
        if "0x" in j:
            hexvalue = j
    if hexvalue == "":
        return "Cannot Parse Value"

    htmltext = ""
    htmltext += '<li><lsc href="#">' + name + '</lsc><ul><li><table>'
    hexvalue, splitindex, fieldlabels = rns_decoder.decodeval(hexvalue, node.start, node.end)

    szreg = len(hexvalue)
    sz = 0
    grupno = 1
    print node.subfields
    while sz < szreg:
        sztemp = sz
        htmltext += "<tr>"
        grupnotemp = grupno
        while True:
            if fieldlabels[sztemp] == 0:
                cellid = str(i) + '-' + str(0)
                htmltext += '<th id="' + cellid + '" bgcolor="#f9cccc" onmouseover="descppopup(this, event)" onmouseout="descppopupblck()"">' + str(splitindex[sztemp]-1) + "-" + str(splitindex[sztemp+1]) + "</th>"
            else:
                cellid = str(i) + '-' + str(grupnotemp)
                grupnotemp += 1
                if sztemp%2 == 0:
                    htmltext += '<th id="' + cellid + '" onmouseover="descppopup(this, event)" onmouseout="descppopupblck()"">' + str(splitindex[sztemp]) + "-" + str(splitindex[sztemp+1]-1) + "</th>"
                else:
                    htmltext += '<th id="' + cellid + '" bgcolor="#dddddd" onmouseover="descppopup(this, event)" onmouseout="descppopupblck()"">' + str(splitindex[sztemp]-1) + "-" + str(splitindex[sztemp+1]) + "</th>"
            sztemp += 1
            if sztemp%6 == 0 or sztemp == szreg:
                break

        htmltext += "</tr><tr>"
        sztemp = sz
        grupnotemp = grupno
        while True:
            if fieldlabels[sztemp] == 0:
                cellid = str(i) + '-' + str(0)
                htmltext += '<th id="' + cellid + '" bgcolor="#f9cccc" onmouseover="descppopup(this, event)" onmouseout="descppopupblck()"">' + "RESERVED" + "</th>"
            else:
                cellid = str(i) + '-' + str(grupnotemp)
                grupnotemp += 1
                if sztemp%2 == 0:
                    htmltext += '<th id="' + cellid + '" onmouseover="descppopup(this, event)" onmouseout="descppopupblck()"">' + node.subfields[len(node.subfields)-grupnotemp] + "</th>"
                else:
                    htmltext += '<th id="' + cellid + '" bgcolor="#dddddd" onmouseover="descppopup(this, event)" onmouseout="descppopupblck()"">' + node.subfields[len(node.subfields)-grupnotemp] + "</th>"
            sztemp += 1
            if sztemp%6 == 0 or sztemp == szreg:
                break

        htmltext += "</tr><tr>"
        sztemp = sz
        grupnotemp = grupno
        while True:
            if fieldlabels[sztemp] == 0:
                cellid = str(i) + '-' + str(0)
                htmltext += '<td id="' + cellid + '" bgcolor="#f9cccc" onmouseover="descppopup(this, event)" onmouseout="descppopupblck()"">' + "0x" + hexvalue[sztemp] + "</td>"
            else:
                cellid = str(i) + '-' + str(grupnotemp)
                grupnotemp += 1
                if sztemp%2 == 0:
                    htmltext += '<td id="' + cellid + '" onmouseover="descppopup(this, event)" onmouseout="descppopupblck()"">' + "0x" + hexvalue[sztemp] + "</td>"
                else:
                    htmltext += '<td id="' + cellid + '" bgcolor="#dddddd" onmouseover="descppopup(this, event)" onmouseout="descppopupblck()"">' + "0x" + hexvalue[sztemp] + "</td>"
            sztemp += 1
            if sztemp%6 == 0 or sztemp == szreg:
                break

        htmltext += '</tr>'
        sz = sztemp
        grupno = grupnotemp
    htmltext += "</table></li></ul></li>\n"
    return htmltext

def GetTreeHtml(head, name, outputdict):
	htmltext = ""
	if (head.child != []):
		htmltext +=  '<li><lsc href="#">' + head.name + '</lsc>'
		for i in head.child:
			nametemp = name			
			nametemp += '.'+ i.name
			if nametemp in outputdict.keys():
				htmltext += "<ul>"
				htmltext += GetTreeHtml(i, nametemp, outputdict)
				htmltext += "</ul>"
		htmltext += "</li>"
	else:
		print name, outputdict[name]
		if '0x' in outputdict[name]:
			htmltext += ParseSingleRegisterValue([outputdict[name]], head, head.name, 0)
	return htmltext

def ParseTreeRegisterValue(comnddump, node, name, i):
	outputdict = {}
	for i in comnddump:
		if '0x' in i:
			i.replace('/r','')
			i = ' '.join(i.split())
			j = i.split(' ')
			print j
			outputdict[j[3]] = j[2]

	htmltext = ""
	htmltext = GetTreeHtml(node, name, outputdict)
	print htmltext
	return htmltext

def query(websock, mssg):
    ans = ['r']
    decodedvalue = []
    decodedvaluelabel = []
    decodedsize = []
    htmltext = ""
    for i in range(len(clients[id(websock)]["RequestHeads"])):
        if clients[id(websock)]["RegisterQuery"][i] != []:
            ii = clients[id(websock)]["RegisterQuery"][i]
            ii = ".".join(ii)
            if clients[id(websock)]["RegisterType"][i] == 'l':
                Router.Cmd("read " + ii + " -v0")
            else:
                Router.Cmd("read " + ii + " -family -zero")
            time.sleep(0.1)
            answ = Router.Read()
            if "read: No Such Register:" in answ:
                print "NULL"
                decodedvalue.append([])
                decodedvaluelabel.append([])
                decodedsize.append([])
            else:
                answ = answ.split('\n')
                if clients[id(websock)]["RegisterType"][i] == 'l':
                    htmltext += ParseSingleRegisterValue(answ, clients[id(websock)]["RequestHeads"][i], ii, i)
        else:
            decodedvalue.append([])
            decodedvaluelabel.append([])
            decodedsize.append([])

    ans.append("")
    ans = ",".join(ans)
    websock.write_message(ans)

def free(websock, mssg):
    clients[id(websock)]["head"] = pyc.Head
    clients[id(websock)]["RegisterQuery"] = []
    clients[id(websock)]["RequestHeads"] = []
    clients[id(websock)]["RegisterType"] = []

def know(websock, mssg):
    mssg = mssg[1].split('-')
    node = clients[id(websock)]["RequestHeads"][int(mssg[0])]
    if mssg[1] == '0':
        msg = "k," + node.name + "," + node.startaddr + "," + "RESERVED" + "," + "RESERVED" + "," + "RESERVED"
    else:
        msg = "k," + node.name + "," + node.startaddr + "," + node.subfields[int(mssg[1])-1] + "," + node.subfield_desc[int(mssg[1])-1]
    websock.write_message(msg)

options = {"i" : init,
           "s" : suggst,
           "d" : goup,
           "a" : add,
           "r" : remv,
           "q" : query,
           "e" : exitrouter,
           "f" : free,
           "k" : know,}

class UI_Handler(tornado.web.RequestHandler):
    def get(self):
        print "[INF] [HTML] Page request for UI/index.html"
        self.render("UI/index.html")

class UI_Handler2(tornado.web.RequestHandler):
    def get(self, input):
        print "[INF] [HTML] Page request for UI/index.html"
        self.render("UI/" + input + ".html")

class Task_2_WebSocketHandler(tornado.websocket.WebSocketHandler):
    def open(self):
        print "[INF] [SOCK] New client connected"

    def on_message(self, message):
        print "[INF] [SOCK] New message recieved"
        message = message.encode('ascii', 'ignore')
        messagesplit = re.split(',|-|_|\n', message)
        message = ""
        print "[INF] [SOCK] Incoming request ", messagesplit
        self.write_message(message)

    def on_close(self):
        self.application.manager.remove_web_server(self)

class LogIn_WebSocketHandler(tornado.websocket.WebSocketHandler):
    def open(self):
        print "[INF] [SOCK] New client connected"

    def on_message(self, message):
        print "[INF] [SOCK] New message recieved"
        message = message.encode('ascii', 'ignore').split(',')

        status = Router.Connect(message[1], message[2], message[3])
        if status == 'Success':
            print "[INF] [ROUT] Login Success"
            status = Router.BringUp()
            if status == 'Success':
                print "[INF] [ROUT] Shell bringup success"
                Router.Cmd("cd /harddisk")
                Router.Cmd("./yoda_ra_static")
                self.write_message('done')
            else:
                print "[INF] [ROUT] Error Shell bringup fail"
                self.write_message(status)
        else:
            print "[INF] [ROUT] Error Login"
            self.write_message(status)

class DC_WebSocketHandler(tornado.websocket.WebSocketHandler):
    def open(self):
        print "[INF] [SOCK] [DC] New client connected ", id(self)
        clients[id(self)] = {}
        clients[id(self)]["head"] = pyc.Head
        clients[id(self)]["RegisterQuery"] = []
        clients[id(self)]["RequestHeads"] = []
        clients[id(self)]["RegisterType"] = []

    def on_message(self, message):
        print "[INF] [SOCK] [DC] New message recieved ", id(self)
        message = message.encode('ascii', 'ignore')
        message = message.split(',')
        options[message[0]](self, message)

    def on_close(self):
        del clients[id(self)]
        print '[INF] [SOCK] [DC] Connection closed ', id(self)

class DC_COM_WebSocketHandler(tornado.websocket.WebSocketHandler):
    def open(self):
        print "[INF] [SOCK] [DC] [COM] New client connected ", id(self)
        clients[id(self)] = {}

    def on_message(self, message):
        print "[INF] [SOCK] [DC] New message recieved ", id(self)
        message = message.encode('ascii', 'ignore')
        message = message.split(',')

    def on_close(self):
        del clients[id(self)]
        print '[INF] [SOCK] [DC] Connection closed ', id(self)

class DB_Handler(tornado.web.RequestHandler):
    def get(self, input):
        print "[INFO] Page request for task 2 with file "+input+".html"
        self.render("DB/"+input+".html")

class DC_Handler(tornado.web.RequestHandler):
    def get(self, input):
        print "[INFO] Page request for task 2 with file "+input+".html"
        self.render("DC/"+input+".html")

# Main entry function for program
#
# Input Arguments : optional port number
#
if __name__ == "__main__":
    pyc = rns_database.RegisterAsic("luke_registers_reg_list.txt")
    Router = rns_router.Router()
    # Handlers for all HTTP requests
    handlers = [(r"/", UI_Handler),
                (r"/UI/(\w+)", UI_Handler2),
                (r"/DB/(\w+)", DB_Handler),
                (r"/DC/(\w+)", DC_Handler),
                (r'/LogInwebsocket', LogIn_WebSocketHandler),
                (r'/DCwebsocket', DC_WebSocketHandler)]

    # Settings for server
    settings = {'debug' : True, 'template_path': 'rns_dir', 'static_path' : os.path.join(os.path.dirname(__file__), "rns_dir")}
    app = tornado.web.Application(handlers, **settings)
    http_server = tornado.httpserver.HTTPServer(app)

    if len(os.sys.argv) < 2: # If user has entered
        http_server.listen(8090)
        print "[ERR] No port number specified, please execute: python <filename> <port number>"
        print "[INF] Starting server at port 8090"
    else:
        http_server.listen(int(os.sys.argv[1]))
        print "[INF] Starting server at port " + os.sys.argv[1]
    tornado.ioloop.IOLoop.instance().start()
