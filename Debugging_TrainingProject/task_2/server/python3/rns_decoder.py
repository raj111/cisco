import math # used for math.ceil()

# decodeval() function is used to decode hex value read from router into bit fields.
# Algorithm : Convert HEX to BINARY
#             split binary into bitfields according to 'start' and 'end' index
#             Append 0s at beginnning of each BIT FIELD to make length multiple of 4 for stable HEX conversion
#             Convert each BIT FIELD from BINARY to HEX
#             Label each BIT FIELD (to recognize reserved bit fields)
#
# Input Arguments : [String] hex value from router
#                   [List of int] containing bit-field starting index
#                   [List of int] containing bit-field ending index
#
# Output Arguments : [List of str] containing bit-field information HEX
#                    [List of int] contains bit field indices in shorted manner
#                    [List of int] contains bit field labels
#
def decodeval(hexvaluetemp, start, end):
    end = [x+1 for x in end]
    hexvalue = hexvaluetemp[hexvaluetemp.index('x')+1::] # remove leading '0x'
    hexvalue = hexvalue.replace('\r', "").replace('\n', "") # remove unwanted characters
    length = (len(hexvalue))*4 # find length of register
    binaryvalue = ((bin(int(hexvalue, 16))[2:]).zfill(length))[::-1] # convert to binary as little endian
    splitindex = sorted(list(set([0] + (start + end)+[len(binaryvalue)]))) # contains indices to split binary value
    bitfields = [binaryvalue[splitindex[i]:splitindex[i+1]][::-1] for i in range(len(splitindex)-1)[::-1]] # actually split into fields
    bitfields = [hex(int(i, 2))[2:].zfill(int(math.ceil(len(i)/4.0))) for i in bitfields] # append leading zeros if needed
    bitfieldlabels = []
    count = 1
    # label bit fields
    for i in range(len(splitindex)-1):
        if splitindex[i] in start: # non reserved bit-field
            bitfieldlabels = [count] + bitfieldlabels
            count += 1
        else: # reserved bit-field
            bitfieldlabels = [0] + bitfieldlabels
    return bitfields, splitindex[::-1], bitfieldlabels

# Unit test code for decoder module
# expected result
# (['000000', '1a', '2'], [32, 8, 2, 0], [0, 1, 0])
# (['000', '22', '000', '08'], [32, 22, 16, 5, 0], [0, 2, 0, 1])
# (['00000000'], [32, 0], [1])
# (['0000', '000000000400'], [64, 48, 0], [0, 1])
#
if __name__ == "__main__":
    print decodeval("0x0000006a", [2], [7])
    print decodeval("0x00220008", [0, 16], [4, 21])
    print decodeval("0x00000000", [0], [31])
    print decodeval("0x0000000000000400", [0], [47])
else:
    pass
