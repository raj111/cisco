import time # used for delay
import paramiko # used for communication

# Router class handles communication with router via SSH session.
# Data Members : SSHVar - variable containing SSH session
#                ChannelVar - variable for shell in SSH shell
#
# Member Functions : Router - Constructor for class
#                    Connect - Creates SSH connection with router
#                    BringUp - Opens interactive shell in SSH session
#                    Cmd - Registers new terminal command in router
#                    Read - Reads router terminal dump
#                    Close - Closes connection
#
class Router:
    def __init__(self):
        self.SSHVar = paramiko.SSHClient()
        self.SSHVar.set_missing_host_key_policy(paramiko.AutoAddPolicy())

# Connect() function is used to connect to router via SSH
#
# Input Arguments : [String] host name/ip
#                   [String] login username
#                   [String] login password
#
# Output Arguments : [String] error message if unsuccessfull else "Success"
#
    def Connect(self, hostn, usern, passw):
        try:
            self.SSHVar.connect(hostn, username=usern, password=passw)
        except Exception, error:
            return str(error)
        return "Success"

# BringUp() function is used to create interacitve shell inside ssh session
#
# Input Arguments :
#
# Output Arguments : [String] error message if unsuccessfull else "Success"
#
    def BringUp(self):
        try:
            self.ChannelVar = self.SSHVar.invoke_shell(term='dumb')
        except Exception, error:
            return str(error)
        return "Success"

# Cmd() function is used to send and execute command to router
#
# Input Arguments : [String] command to be executed without new line character
#
# Output Arguments : [String] error message if unsuccessfull else "Success"
#
    def Cmd(self, cmd):
        try:
            self.ChannelVar.send(cmd + '\n')
            time.sleep(0.1)
        except Exception, error:
            return str(error)
        return "Success"

# Read() function is used to read router terminal dump
#
# Input Arguments :
#
# Output Arguments : [String] terminal dump or null
#
    def Read(self):
        buff = ''
        while self.ChannelVar.recv_ready():
            buff += self.ChannelVar.recv(9999)
        return buff

# Close() function is used to close SSH session
#
# Input Arguments :
#
# Output Arguments : [String] error message if unsuccessfull else "Success"
#
    def Close(self):
        try:
            self.SSHVar.close()
        except Exception, error:
            return str(error)
        return "Success"

# Unit test code for router module
# expected result
# ls -l
# [Router_RP_0:~]$ ls -l
# total 0
# [Router_RP_0:~]$ cd /harddisk
# [Router_RP_0:/harddisk]$
#
if __name__ == "__main__":
    router = Router()
    if router.Connect("10.104.33.159", "root", "lab") == "Success":
        if router.BringUp() == "Success":
            router.Cmd("ls -l")
            router.Cmd("cd /harddisk")
            print router.Read()

    router.Close()
else:
    pass
