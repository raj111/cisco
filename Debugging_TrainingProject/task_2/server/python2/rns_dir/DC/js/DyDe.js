var RequestedRegisters = []; // used in Complete.js
var Websocket = []; //
var RegisterQueue = []; // used for aggregagted name
var ListRegisterQueue = [];
var TotalRegistersQueue = 0;
var mouse = [];

function onBodyLoad()
{
	var url = document.URL.replace("http", "ws");
	url = url.replace(url.substring(url.indexOf('/', 5), url.length), "/") + "DCwebsocket";
	//console.log(url);
	Websocket = new WebSocket(url);
	console.log("[INF] [SOCK] Socket Connection Created");
	Websocket.onmessage = function (e){recieveMsg(e.data);}
	setTimeout(function(){ sendMsg("i"); }, 1000);
	RegisterQueue.push("");
}

function recieveMsg(msg)
{
	msg = msg.split(",");
	
	switch (msg[0]) {
		case "s":
				msg.shift();
				rs.options = msg;
	
				if(msg.length == 0){
					document.getElementById("input").style.backgroundColor = '#000';
				}
		    break;
		case "a":
				msg.shift();
				var newdiv = document.createElement('div');
				newdiv.id = TotalRegistersQueue.toString() + 'q';
				newdiv.addEventListener("click", function(){deletequeuenodes(this);});
				var txt = RegisterQueue.join(".");
				ListRegisterQueue.push(txt);
				console.log("RegisterQueue; after addition in submission queue " + ListRegisterQueue);
				RegisterQueue = [""];
				if(msg[0] == "l") {
						newdiv.innerHTML = '<span style="color: blue">[LEAF]</span> ' + txt;
				} else {
						newdiv.innerHTML = '<span style="color: green">[TREE]</span> ' + txt;
				}
				TotalRegistersQueue = TotalRegistersQueue + 1;
				document.getElementById("contentright").appendChild(newdiv);
				var myNode = document.getElementById("0");
				deletenodes(myNode);
			break;
		case "r":
				msg.shift();
				document.getElementById("treeid").innerHTML = msg;
				addclicktoresultlist();
				document.getElementById("myModal").style.display = "block";
			break;
		case 'k':
				msg.shift();
				console.log(msg);
				document.getElementById("regname").innerHTML = msg[0];
				document.getElementById("regaddr").innerHTML = msg[1];
				document.getElementById("fldalias").innerHTML = msg[2];
				document.getElementById("flddesc").innerHTML = msg[3];
				document.getElementById("descppop").style.left = (mouse[0] + 20).toString()+"px";
				document.getElementById("descppop").style.top = (mouse[1] + 20).toString()+"px";
				document.getElementById("descppop").style.display = "block";
				document.getElementById("descppop").style.zIndex = "2";
		    break;
	}
}

function sendMsg(optxt) 
{
	Websocket.send(optxt);
}

var treelevel = 1;

function DisplayTree(value)
{
	var newdiv = document.createElement('div');
	newdiv.id = treelevel.toString();
	var txt = "&nbsp;".repeat(4*treelevel);
	txt = txt + "|-" + value;
	newdiv.innerHTML = txt;
	newdiv.addEventListener("click", function(){deletenodes(this);});
	document.getElementById("contentleft").appendChild(newdiv);
	treelevel = treelevel + 1;
}

function deletenodes(elemnt)
{
	var start = treelevel-1;
	var end = parseInt(elemnt.id);
	var element = "d" + "," + (start-end).toString();
	sendMsg(element);
	while(start != end) {
		element = document.getElementById(start.toString());
		element.parentNode.removeChild(element);
		start = start - 1;
		document.getElementById("input").style.backgroundColor = '#fff';
	}
	treelevel = start + 1;
}

function deletequeuenodes(element)
{
	element.parentNode.removeChild(element);
	id = element.id;
	//console.log("removed node from submit queue " + id);
	id = id.slice(0, -1);
	ListRegisterQueue[id] = '';
	console.log("RegisterQueue after deletion in submission queue " + ListRegisterQueue);
	id = "r" + "," + id.toString();
	sendMsg(id);
	
}

function AddRegisterQueue()
{
	var msg = "a" + "," + RegisterQueue.join(",");
	sendMsg(msg);
}

function SubmitRegisterQueue()
{
	var msg = ["q"];
	sendMsg(msg);
	
	while(TotalRegistersQueue>0) {
		TotalRegistersQueue = TotalRegistersQueue -1;
		var id = TotalRegistersQueue.toString() + "q";
		var elem = document.getElementById(id);
		if(elem != null) {
			elem.parentNode.removeChild(elem);
		}
	}

}

function DisplayRegisterValue(msg)
{
	ListRegisterQueue = ListRegisterQueue.filter(function(v){return v!==''});
	for(var i=0; i<ListRegisterQueue.length; i++) {
		console.log(msg[i] + " - " + ListRegisterQueue[i]);
	}
}


function LogOff()
{
	var msg = "e";
	sendMsg(msg);
}

function descppopup(element, event)
{
	var msg = "k" + "," + element.id;
	sendMsg(msg);
	mouse[0] = event.clientX;
	mouse[1] = event.clientY;
	console.log(element.id + " " + event.clientX + " " + event.clientY);
	
}

function descppopupblck()
{
	document.getElementById("descppop").style.display = "none";
}
