var WebSocket; // Websocket object

// OnSubmit() function is used to validate user input locally then send it to server
//
// Input Arguments : 
//
// Output Arguments : 
//
function OnSubmit()
{
	var data;
	var formdata = [];
	var formvalidity = true;

	data = document.getElementById("rddnfld").value;
	if(data == []) {
		document.getElementById("rddntxt").style.color = "red";
		formvalidity = false;
	}
	formdata.push(data);

	data = document.getElementById("rdipfld").value;
	if (!(/^[0-9.]+$/.test(data))) {
		document.getElementById("rdiptxt").style.color = "red";
		formvalidity = false;
	}
	formdata.push(data);

	data = document.getElementById("rdunfld").value;
	if (data == []) {
		document.getElementById("rduntxt").style.color = "red";
		formvalidity = false;
	}
	formdata.push(data);

	data = document.getElementById("rdpwfld").value;
	if (data == []) {
		document.getElementById("rdpwtxt").style.color = "red";
		formvalidity = false;
	}
	formdata.push(data);

	if (formvalidity == false) {
		document.getElementById("contentwarning").innerHTML = 'Incomplete Input';
		document.getElementById("contentwarning").style.display = "block";
		setTimeout(HideError, 4000);
	} else {
		console.log(formdata);
		sendMsg(formdata);
	}
}

// HideError() function is used to reset the form fields after error is displayed
//
// Triggering Event : 
//
// Input Arguments : 
//
// Output Arguments : 
//
function HideError() 
{
	document.getElementById("contentwarning").style.display = "none";
	document.getElementById("rddntxt").style.color = "black";
	document.getElementById("rdiptxt").style.color = "black";
	document.getElementById("rduntxt").style.color = "black";
	document.getElementById("rdpwtxt").style.color = "black";
}

// onLoad() function is used to establish websocket connection with server
//
// Triggering Event : On HTML page load
//
// Input Arguments : 
//
// Output Arguments : 
//
function onLoad()
{
	var url = document.URL.replace("http", "ws");
	url = url.replace(url.substring(url.indexOf('/', 5), url.length), "/") + "LogInwebsocket";
	console.log(url);
	WebSocket = new WebSocket(url);
	console.log("[INF] [SOCK] Socket Connection Created");
	WebSocket.onmessage = function (e){recieveMsg(e.data);}
}

// recieveMsg() function is used to display error or success message recieved from server
//
// Triggering Event : Server sends message after trying to connect to router
//
// Input Arguments : Message from server
//
// Output Arguments : 
//
function recieveMsg(msg)
{
	if (msg == 'done') {
		window.location="/UI/DCDB";

	} else {
		document.getElementById("contentwarning").innerHTML = msg;
		document.getElementById("contentwarning").style.display = "block";
		setTimeout(HideError, 4000);
	}
}

// sendMsg() function is used to send message to server via websocket
//
// Triggering Event : 
//
// Input Arguments : message to be sent
//
// Output Arguments : 
//
function sendMsg(optxt) 
{
	WebSocket.send(optxt);
}

