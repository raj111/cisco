import tornado.web
import tornado.websocket
import tornado.httpserver
import tornado.ioloop

import re
import sys

# DataBase
data = {'RegisterA':'0xAAAA',
		'RegisterB':'0xBBBB',
		'RegisterC':'0xCCCC',
		'RegisterD':'0xDDDD'};
  
# Handles communication with client
class WebSocketHandler(tornado.websocket.WebSocketHandler):
	# Function is called when new connection is created    
    def open(self):
        pass

	# Function is called when server recieves a message
	# ToDo : Multi-client message segregation according to client
	#	   : Multiple register request
    def on_message(self, message):
        message = message.encode('ascii','ignore')
        messagesplit = re.split(',|-|_|\n', message)
	#print messagesplit[0]
        for i in messagesplit:
            print i

	#self.write_message(data[message]);
        if message in data.keys():
  	    value = data[message]
        else:
  	    value = "Invalid Register"
	
        print message, value
        self.write_message(value)
 
	# Function is called when connection with client is closed
    def on_close(self):
        pass
 
 

class IndexPageHandler(tornado.web.RequestHandler):
    def get(self, input):
	print input
        self.render("index.html")
 
 
class Application(tornado.web.Application):
    def __init__(self):
        handlers = [
            (r'/(\w+)', IndexPageHandler),
            (r'/websocket', WebSocketHandler)
        ]
 
        settings = {
            'template_path': 'pages'
        }
#        tornado.web.Application.__init__(self, handlers, **settings)
        tornado.web.Application.__init__(self, handlers) 
 
if __name__ == '__main__':
    portnumb = sys.argv[1]
    ws_app = Application()
    server = tornado.httpserver.HTTPServer(ws_app)
    server.listen(portnumb)
    print "server opened on local cost and port number ", portnumb
    tornado.ioloop.IOLoop.instance().start()
