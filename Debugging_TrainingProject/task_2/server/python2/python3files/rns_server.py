#import textwrap
import tornado.httpserver
import tornado.ioloop
import tornado.options
import tornado.web
import tornado.websocket
import time

#import sys
import re
import os

#from rns_pyATSdummy import Connect2Router
#import rns_database
import parseluke
import rns_router
import rns_decoder

pyc = []
clients = {}
Router = []
'''
			<li><lsc href="#">Part 1</lsc>
				<ul>
				<li>
					<table>
	  					<tr>
							<th>Co</th>
							<th>Co</th>
	  					</tr>
	  					<tr>
							<td>Al</td>
							<td>Ma</td>
						</tr>
					</table>
				</li>
				</ul>
			</li>
'''

def createoutput(namelist, valuelist, splitindexlist, sizelist, nodelist):
	htmltext = ""
	for i in range(len(namelist)):
		if valuelist[i] != [] and valuelist[i][0] != "NULL":
			htmltext = htmltext + '<li><lsc href="#">' + ".".join(namelist[i]) + '</lsc><ul><li><table>'
			htmltext = htmltext + '<tr>\n'
			print splitindexlist[i]
			for j in range(len(splitindexlist[i])-1):
				htmltext = htmltext + "<th>" + str(splitindexlist[i][j]) + "-" + str(splitindexlist[i][j+1]-1) + "</th>\n"
			htmltext = htmltext + "</tr>\n"
			htmltext = htmltext + '<tr>\n'
			#print splitindexlist[i]
			for j in range(len(splitindexlist[i])-1):
				htmltext = htmltext + "<td>" + "0x" + valuelist[i][j] + "</td>\n"
			htmltext = htmltext + "</tr>\n"
			htmltext = htmltext + "</table></li></ul></li>\n"

	print htmltext
	return htmltext
				
				
def init(websock, msg):
	print clients[id(websock)]["head"].name
	msg = ["s"] + clients[id(websock)]["head"].childnames;
#	msg = msg.append(pyc.CurrentPointer.childnames);
	msg = ",".join(msg)
	websock.write_message(msg)

def suggst(websock, mssg):
	indx = clients[id(websock)]["head"].childnames.index(mssg[1])
	clients[id(websock)]["head"] = clients[id(websock)]["head"].child[indx]
	msg = ["s"] + clients[id(websock)]["head"].childnames;
	msg = ",".join(msg)
	websock.write_message(msg)

def goup(websock, mssg):
	step = int(mssg[1])
	for i in range(step):
		clients[id(websock)]["head"] = clients[id(websock)]["head"].parent
	msg = ["s"] + clients[id(websock)]["head"].childnames;
	msg = ",".join(msg)
	websock.write_message(msg)

def add(websock, mssg):
	#print mssg
	mssg.pop(0)
	#print mssg
	clients[id(websock)]["RegisterQuery"].append(mssg)
	clients[id(websock)]["RequestHeads"].append(clients[id(websock)]["head"])
	if(clients[id(websock)]["head"].childnames == []):
		mssg = ["a"] + ["l"] + mssg
	else:
		mssg = ["a"] + ["t"] + mssg
	mssg = ",".join(mssg)
	websock.write_message(mssg)
	
	#print clients[id(websock)]["RegisterQuery"]

def remv(websock, mssg):
	clients[id(websock)]["RegisterQuery"][int(mssg[1])] = []
	clients[id(websock)]["RequestHeads"][int(mssg[1])] = []	
	#print clients[id(websock)]["RegisterQuery"]

def query(websock, mssg):
	ans = ['r']
	decodedvalue = []
	decodedsplitindex = []
	decodedsize = []
	for i in range(len(clients[id(websock)]["RequestHeads"])):
		#print "hit ", i
		if (clients[id(websock)]["RegisterQuery"][i] != []):
			ii = clients[id(websock)]["RegisterQuery"][i]
			ii = ".".join(ii)
			#print "[INF] [ROUT] Register requested ", ii, " ", clients[id(websock)]["RequestHeads"][i].name, " ", clients[id(websock)]["RequestHeads"][i].size, " ", clients[id(websock)]["RequestHeads"][i].start, " ", clients[id(websock)]["RequestHeads"][i].end
			Router.Cmd("read " + ii + " -v0")
			time.sleep(0.1)
			answ =  Router.Read()
			if "read: No Such Register:" in answ:
				print "NULL"
				decodedvalue.append([])
				decodedsplitindex.append([])
				decodedsize.append([])
			else:
				answ = answ.split('\n')
				#print answ
				for j in answ:
					if "0x" in j:
						#print j
						#print clients[id(websock)]["RequestHeads"][i].start
						#print clients[id(websock)]["RequestHeads"][i].end
						bitfields, splitindex, size = rns_decoder.decodeval(j, clients[id(websock)]["RequestHeads"][i].start, clients[id(websock)]["RequestHeads"][i].end)
						decodedvalue.append(bitfields)
						decodedsplitindex.append(splitindex)
						decodedsize.append(size)
						break;
		else:
			decodedvalue.append([])
			decodedsplitindex.append([])
			decodedsize.append([])
				

	htmltext = createoutput(clients[id(websock)]["RegisterQuery"], decodedvalue, decodedsplitindex, decodedsize, clients[id(websock)]["RequestHeads"])
	
	ans.append(htmltext)
	ans = ",".join(ans)
	clients[id(websock)]["head"] = pyc.Head
	clients[id(websock)]["RegisterQuery"] = []
	clients[id(websock)]["RequestHeads"] = []
	print ans
	websock.write_message(ans)

options = {"i" : init,
           "s" : suggst,
		   "d" : goup,
		   "a" : add,
		   "r" : remv,
		   "q" : query,}


#============================================================================================================
# Handles all requests for UI
class UI_Handler(tornado.web.RequestHandler):
	# Gets executed when client enters task 2
	def get(self):
		print "[INF] [HTML] Page request for UI/index.html"
		self.render("UI/index.html")

#============================================================================================================
# Handles all requests for UI
class UI_Handler2(tornado.web.RequestHandler):
	# Gets executed when client enters task 2
	def get(self, input):
		print "[INF] [HTML] Page request for UI/index.html"
		self.render("UI/" + input + ".html")

# Handles communication with client
class Task_2_WebSocketHandler(tornado.websocket.WebSocketHandler):
	# Function is called when new connection is created
    def open(self):
		print "[INF] [SOCK] New client connected"

	# Function is called when server recieves a message
	# ToDo : Multi-client message segregation according to client
	#	   : Multiple register request
    def on_message(self, message):
		print "[INF] [SOCK] New message recieved"
		message = message.encode('ascii','ignore')
		messagesplit = re.split(',|-|_|\n', message)
		message = ""
		print "[INF] [SOCK] Incoming request ", messagesplit
		#for i in messagesplit:
	#		if i in data.keys():
#				message += data[i] + ","
#			else:
#				message += "Invalid Register,"
#
#		message = message[:-1]
#		print "[INF] [SOCK] [DB] Outgoing answer " + message

		self.write_message(message)

		# Function is called when connection with client is closed
    def on_close(self):
		self.application.manager.remove_web_server(self)

# Handles communication with client
class LogIn_WebSocketHandler(tornado.websocket.WebSocketHandler):
	# Function is called when new connection is created
	def open(self):
		print "[INF] [SOCK] New client connected"

	def on_message(self, message):
		print "[INF] [SOCK] New message recieved"
		message = message.encode('ascii','ignore').split(',')

		status = Router.Connect(message[7], message[8], message[9])
		if(status == 'Success'):
			print "[INF] [ROUT] Login Success"
			status = Router.BringUp()
			if(status == 'Success'):
				print "[INF] [ROUT] Shell bringup success"
				Router.Cmd("cd /harddisk")
				print Router.Read()
				Router.Cmd("./yoda_ra_static")
				print Router.Read()
				self.write_message('done')
			else:
				print "[INF] [ROUT] Error Shell bringup fail"
				self.write_message(status)
		else:
			print "[INF] [ROUT] Error Login"
			self.write_message(status)
	
#		print "[INF] [ROUT] New message recieved"		
		#print status
#		self.write_message("Success")		
#		self.write_message(status)

#		self.write_message("done")		


# Handles communication with client
class DC_WebSocketHandler(tornado.websocket.WebSocketHandler):
	# Function is called when new connection is created
	def open(self):
		print "[INF] [SOCK] [DC] New client connected ", id(self)
		clients[id(self)] = {}
		clients[id(self)]["head"] = pyc.Head
		clients[id(self)]["RegisterQuery"] = []
		clients[id(self)]["RequestHeads"] = []
		print clients[id(self)]["RegisterQuery"]
		#options["i"](self, "")
#		print id(self)

	def on_message(self, message):
		print "[INF] [SOCK] [DC] New message recieved ", id(self)
		message = message.encode('ascii','ignore')
		message = message.split(',')		
		options[message[0]](self, message)
		#print status
		#self.write_message(status)

	def on_close(self):
		del clients[id(self)]
		print '[INF] [SOCK] [DC] Connection closed ', id(self)

#============================================================================================
# Handles all requests for task 3
class DB_Handler(tornado.web.RequestHandler):
	# Gets executed when client enters task 3
	def get(self, input):
		print "[INFO] Page request for task 2 with file "+input+".html"
		self.render("DB/"+input+".html")

# Handles all requests for task 3
class DC_Handler(tornado.web.RequestHandler):
	# Gets executed when client enters task 3
	def get(self, input):
		print "[INFO] Page request for task 2 with file "+input+".html"
		self.render("DC/"+input+".html")



class WrapHandler(tornado.web.RequestHandler):
	def post(self):
		text = self.get_argument('text')
		width = self.get_argument('width', 40)
		self.write(textwrap.fill(text, int(width)))



if __name__ == "__main__":

#	pyc = rns_database.RegisterAsic("amin.js")
	pyc = parseluke.LukeRegisters("luke_registers_reg_list.txt")
	Router = rns_router.Router()

	# Task 2 can be invoked by http://<localhost>:<port>/task2/<name of html file>, 
	# ex http://localhost:8090/task2/index will open index.html from pages folder
	# Task_2_Handler handles all task 2 request.
	#
	# Task 3 can be invoked by http://<localhost>:<port>/task3/<name of html file>, 
	# ex http://localhost:8090/task3/index will open index.html from pages folder
	# Task_3_Handler handles all task 3 request.
	#
	# Task 3 can be invoked by http://<localhost>:<port>/task3/<name of html file>, 
	# ex http://localhost:8090/task3/index will open index.html from pages folder
	# Task_3_Handler handles all task 3 request.
	#
	handlers = [(r"/", UI_Handler),
				(r"/UI/(\w+)", UI_Handler2),
				(r"/DB/(\w+)", DB_Handler),
				(r"/DC/(\w+)", DC_Handler),
				(r'/task2_websocket', Task_2_WebSocketHandler),
				(r'/LogInwebsocket', LogIn_WebSocketHandler),
				(r'/DCwebsocket', DC_WebSocketHandler),
				(r"/wrap", WrapHandler)]

	# Default web directory which contains all html pages
	settings = {'debug':True,'template_path': 'rns_dir','static_path': os.path.join(os.path.dirname(__file__), "rns_dir")}

	# Create tornado application
	app = tornado.web.Application(handlers, **settings)

	# Create tornado webserver hosting following service
	http_server = tornado.httpserver.HTTPServer(app)

	# Allocate port to server
	# Parse command line for port number or set some default port number
	if(len(os.sys.argv) < 2):
		http_server.listen(8090)
		print "[ERR] No port number specified, please execute: python <filename> <port number>"
		print "[INF] Starting server at port 8090"
	else:
		http_server.listen(int(os.sys.argv[1]))
		print "[INF] Starting server at port " + os.sys.argv[1]		
	# Start server
	tornado.ioloop.IOLoop.instance().start()



