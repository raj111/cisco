var ErrorLists = ['Incomplete Input'];
var WebSocket;

function OnSubmit()
{
	//console.log("hi");
	var data;
	var formdata = [];
	var formvalidity = true;

	data = document.getElementById("ihdipfld").value;
	if (!(/^[0-9.]+$/.test(data))) {
		document.getElementById("ihdiptxt").style.color = "orange";
		data = [];
	}
	formdata.push(data);

	data = document.getElementById("ihdunfld").value;
	if(data == []) {
		document.getElementById("ihduntxt").style.color = "orange";
	}
	formdata.push(data);

	data = document.getElementById("ihdpwfld").value;
	if(data == []) {
		document.getElementById("ihdpwtxt").style.color = "orange";
	}
	formdata.push(data);

	data = document.getElementById("pdcpfld").value;
	if (!(/^[0-9]+$/.test(data))) {
		document.getElementById("pdcptxt").style.color = "red";
		formvalidity = false;
	}
	formdata.push(data);

	data = document.getElementById("pdapfld").value;
	if (!(/^[0-9]+$/.test(data))) {
		document.getElementById("pdaptxt").style.color = "red";
		formvalidity = false;
	}
	formdata.push(data);

	data = document.getElementById("pdmpfld").value;
	if (!(/^[0-9]+$/.test(data))) {
		document.getElementById("pdmptxt").style.color = "red";
		formvalidity = false;
	}
	formdata.push(data);

	data = document.getElementById("rddnfld").value;
	if(data == []) {
		document.getElementById("rddntxt").style.color = "red";
		formvalidity = false;
	}
	formdata.push(data);

	data = document.getElementById("rdipfld").value;
	if (!(/^[0-9.]+$/.test(data))) {
		document.getElementById("rdiptxt").style.color = "red";
		formvalidity = false;
	}
	formdata.push(data);

	data = document.getElementById("rdunfld").value;
	if(data == []) {
		document.getElementById("rduntxt").style.color = "red";
		formvalidity = false;
	}
	formdata.push(data);

	data = document.getElementById("rdpwfld").value;
	if(data == []) {
		document.getElementById("rdpwtxt").style.color = "red";
		formvalidity = false;
	}
	formdata.push(data);

	if(formvalidity == false) {
		document.getElementById("contentwarning").innerHTML = ErrorLists[0];
		document.getElementById("contentwarning").style.display = "block";
		setTimeout(HideError, 4000);
	} else {
		//console.log("done");
		console.log(formdata);
		sendMsg(formdata);
	}
}

function HideError() 
{
	document.getElementById("contentwarning").style.display = "none";
	document.getElementById("ihdiptxt").style.color = "black";
	document.getElementById("ihduntxt").style.color = "black";
	document.getElementById("ihdpwtxt").style.color = "black";
	document.getElementById("pdcptxt").style.color = "black";
	document.getElementById("pdaptxt").style.color = "black";
	document.getElementById("pdmptxt").style.color = "black";
	document.getElementById("rddntxt").style.color = "black";
	document.getElementById("rdiptxt").style.color = "black";
	document.getElementById("rduntxt").style.color = "black";
	document.getElementById("rdpwtxt").style.color = "black";
}

function onLoad()
{
	var url = document.URL.replace("http", "ws");
	url = url.replace(url.substring(url.indexOf('/', 5), url.length), "/") + "LogInwebsocket";
	console.log(url);
	WebSocket = new WebSocket(url);
	console.log("[INF] [SOCK] Socket Connection Created");
	WebSocket.onmessage = function (e){recieveMsg(e.data);}
}

function recieveMsg(msg)
{
	if(msg == 'done') {
		window.location="/UI/DCDB";

	}
	else {
		document.getElementById("contentwarning").innerHTML = msg;
		document.getElementById("contentwarning").style.display = "block";
		setTimeout(HideError, 4000);
	}

}

function sendMsg(optxt) 
{
	WebSocket.send(optxt);
}

