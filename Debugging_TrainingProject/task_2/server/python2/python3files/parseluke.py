#!/usr/bin/python
class Node:
	def __init__(self,name,startaddr,size,parent):
		self.name = name
		self.startaddr = startaddr
		self.size = size
		self.child = []
		self.childnames = []
		self.parent = parent
		self.subfields = []
		self.start = []
		self.end = []
def traverse(node):
	print node.name
        for i in node.child:
        	traverse(i)


class LukeRegisters:
	def __init__(self,filename):
		self.Head = self.createTree(filename)
		self.currentPointer = self.Head
	def createTree(self,filename):
		try:
			file = open(filename,'r')
		except IOError:	
			print "Checkfile"
		head = Node("LUKE_REGISTERS","0000","0000",None)
		count = 0
		current_level = 1
		currentnode = head
		for line in file:
			if(line[0]!='#'):
				if(line[0]=='0'):
					words=line.split()
					address = words[0]
					size = words[1]
					i=0
					while(words[2][i]=='*'):
						i=i+1
					if("#" in words[2]):
						name = words[2].split('#')[0]
					elif("/" in words[2]):
						name = words[2].split('/')[0]		
					if("[" in name):
						name=name.split('[')[0]
					if(i==current_level):
						name = name.replace("*","")
						temp = Node(name,address,size,currentnode)
						currentnode.child.append(temp)	
						currentnode.childnames.append(name)
					elif(i>current_level):
						currentnode = currentnode.child[-1]
						current_level = current_level+1
						name = name.replace("*","")
						temp = Node(name,address,size,currentnode)
                        			currentnode.child.append(temp)
						currentnode.childnames.append(name)
					else:
						while(current_level!=i):
							current_level = current_level-1
							currentnode = currentnode.parent
						name = name.replace("*","")
						temp = Node(name,address,size,currentnode)
			                        currentnode.child.append(temp)
						currentnode.childnames.append(name)
				else:
					words = line.split()
					if(words[0]!="ALIAS:"):
						currentnode.child[-1].subfields.append(words[0])
						length = words[1].split(':')
						currentnode.child[-1].start.append(int(length[0]))
						currentnode.child[-1].end.append(int(length[1]))
		file.close()
#		traverse(head)
		return head
if __name__ == "__main__":
	head = LukeRegisters("luke_registers_reg_list.txt")
