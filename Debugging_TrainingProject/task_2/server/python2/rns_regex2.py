#!/usr/bin/python
class Node:
	def __init__(self,name,startaddr,size,parent):
		self.name = name
		self.startaddr = startaddr
		self.size = size
		self.child = []
		self.childnames = []
		self.parent = parent
		self.subfields = []
		self.start = []
		self.subfield_desc = []
		self.subfield_name = []
		self.end = []

def traverse(node):
	#print node.name
	for j in range(len(node.subfields)):
		for i in node.child:
			traverse(i)

def reverseLists(node):
	node.start.reverse()
	node.end.reverse()
	for i in node.child:
		reverseLists(i)

class LukeRegisters:
	def __init__(self,filename, filename2):
		self.Head = self.createTree(filename, filename2)
		self.currentPointer = self.Head
	def createTree(self,filename, filename2):
		try:
			file = open(filename,'r')
			file1 = open(filename2,'r')
		except IOError:	
			print "Checkfile"
		count = 0
		first = 0
		first1 = 0
		current_level = 1
		for line in file:
			if(line[0]!='#'):
				if(first1<3):
					head = Node("LUKE_REGISTERS","0000","0000",None)
					first1 = first1+1
					currentnode = head
					continue
				if(line[0]=='0'):
					words=line.split()
					address = words[0]
					size = words[1]
					i=0
					while(words[2][i]=='*'):
						i=i+1
					if("#" in words[2]):
						name = words[2].split('#')[0]
					elif("/" in words[2]):
						name = words[2].split('/')[0]		
					if("[" in name):
						name = name.split('[')[0]
					if(i==current_level):
						name = name.replace("*","")
						temp = Node(name,address,size,currentnode)
						currentnode.child.append(temp)
						currentnode.childnames.append(name)
					elif(i>current_level):
						name = name.replace("*","")
						currentnode = currentnode.child[-1]
						current_level = current_level+1 
						temp = Node(name, address, size, currentnode)
						currentnode.child.append(temp)
						currentnode.childnames.append(name)
					else:
						while(current_level!=i):
							current_level = current_level-1
							currentnode = currentnode.parent
						name = name.replace("*","")
						temp = Node(name, address, size, currentnode)
						currentnode.child.append(temp)
						currentnode.childnames.append(name)
				else:
					words = line.split()
					if(words[0]!="ALIAS:"):
						currentnode.child[-1].subfields.append(words[0])
						length = words[1].split(':')
						currentnode.child[-1].start.append(int(length[0]))
						currentnode.child[-1].end.append(int(length[1]))
					line1 = file1.readline()
					if(first==0):
						line1 = file1.readline()
						first=1
					if("reserved" in line1):
						line1=file1.readline() 			#to read description
						line1=file1.readline()		
					temp=line1.split(':')[1]
					currentnode.child[-1].subfield_name.append(temp.split('[')[0])
					line1=file1.readline()
					if(line1.split(':')[1]!=None):
						currentnode.child[-1].subfield_desc.append(line1.split(':')[1].replace('\n',""))
					else:
						currentnode.child[-1].subfield_desc.append(" ")
		file.close()	
		file1.close()
		reverseLists(head)
#		traverse(head)
		return head

if __name__ == "__main__":
	head = LukeRegisters("luke_registers_reg_list.txt")
