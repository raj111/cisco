# python script to create tree structure for procyon chip

import re # for getting sting inside "<>"

class Node:
	def __init__(self, name, address, parent):
		self.name = name
		self.address = address
		self.child = []
		self.childnames = []
		self.parent = parent

class RegisterAsic:
	def __init__(self, filename):
		self.Head = self.CreateTree(filename)
		self.CurrentPointer = self.Head
	def CreateTree(self, filename):
		try:
			file = open(filename, 'r')
		except IOError:
			print "checkfile"

		head = Node("ProcyonRegisterTree","00000", None)
		count = 0
		current_level = 0
		currentnode = head
		for line in file:
			if (count>-1):
				count=count+1
			i=0
			while(line[i]==' '):
				i=i+1
			words = re.findall(r'\"(.+?)\"', line)
			for word in words:
				if("@" in word):
					wr =  word.split()
					name = wr[0]
					address = wr[1]
			i = i/2 - 1
			if(i==current_level):
#				print "+++ ", name
				temp = Node(name, address, currentnode)
				currentnode.child.append(temp)
				currentnode.childnames.append(name)
			elif(i>current_level):
#				print  currentnode.name," ---> ",currentnode.child[-1].name
				currentnode = currentnode.child[-1]
				current_level = current_level + 1
#				print "+++ ", name
				temp = Node(name, address, currentnode)
				currentnode.child.append(temp)
				currentnode.childnames.append(name)
			else:
				while(current_level != i):
					current_level = current_level - 1
#					print currentnode.parent.name," <--- ", currentnode.name
					currentnode = currentnode.parent
#				print "+++ ", name
				temp = Node(name, address, currentnode)
				currentnode.child.append(temp)
				currentnode.childnames.append(name)
		file.close()
		return head

def GetAddress(head, name):
	print head.name
	print head.childnames
	print ""
	if (head.name == name):
		return head.address
	else:
		for i in head.child:
#			print "\t ul"
			addr = GetAddress(i, name)
			if (addr != None):
				return addr
#	print "\t ul li"

def GetPage(head, level, file):
	level = level + 1
	print "\nli " + head.name + " " + str(level),
	file.write("\nli " + head.name + " " + str(level))
#	print head.child,
	if (head.child != []):
#		print "hi"
#	else:
#		print ""
		print "\nul",
		file.write("\nul")
		for i in head.child:
		#	print
			file = GetPage(i, level, file)
		print "\n-ul",
		file.write("\n-ul")
	print "-li" + str(level),
	file.write("-li" + str(level))
	return file

if __name__ == "__main__":
	pyc = RegisterAsic("amin2.js")
	#print GetAddress(pyc.Head, "hier_insdfsdfsdfstasdasd")
	file = open("hi.txt",'w')
	#GetPage(pyc.Head, 0, file)
	file.close()
else:
	pass

