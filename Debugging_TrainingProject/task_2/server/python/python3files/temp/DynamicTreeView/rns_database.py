# python script to create tree structure for procyon chip

import re # for getting sting inside "<>"

class Node:
	def __init__(self, name, address, parent):
		self.name = name
		self.address = address
		self.child = []
		self.parent = parent

class RegisterAsic:
	def __init__(self, filename):
		self.Head = self.CreateTree(filename)
	def CreateTree(self, filename):
		try:
			file = open(filename, 'r')
		except IOError:
			print "checkfile"

		head = Node("ProcyonRegisterTree","00000", None)
		count = 0
		current_level = 0
		currentnode = head
		for line in file:
			if (count>50000000000000000):
				break
			count=count+1
			i=0
			while(line[i]==' '):
				i=i+1
			words = re.findall(r'\"(.+?)\"', line)
			for word in words:
				if("@" in word):
					wr =  word.split()
					name = wr[0]
					address = wr[1]
			i = i/2 - 1
			if(i==current_level):
#				print "+++ ", name
				temp = Node(name, address, currentnode)
				currentnode.child.append(temp)
			elif(i>current_level):
#				print  currentnode.name," ---> ",currentnode.child[-1].name
				currentnode = currentnode.child[-1]
				current_level = current_level + 1
#				print "+++ ", name
				temp = Node(name, address, currentnode)
				currentnode.child.append(temp)
			else:
				while(current_level != i):
					current_level = current_level - 1
#					print currentnode.parent.name," <--- ", currentnode.name
					currentnode = currentnode.parent
#				print "+++ ", name
				temp = Node(name, address, currentnode)
				currentnode.child.append(temp)
		file.close()
		return head

def GetAddress(head, name):
	print head.name
	if (head.name == name):
		return head.address
	else:
		for i in head.child:
#			print "\t ul"
			addr = GetAddress(i, name)
			if (addr != None):
				return addr
#	print "\t ul li"

def GetPage(head, level, file):
	level = level + 1
	file.write( '<li><input type="checkbox"><a href = "#" onClick = "return false;">' + head.name + "</a>")
#	print head.child,
	if (head.child != []):
#		print "hi"
#	else:
#		print ""
		file.write("\n <ul>")
		for i in head.child:
		#	print
			file = GetPage(i, level, file)
		file.write("\n </ul>")
	file.write("</li>")
	return file

if __name__ == "__main__":
	pyc = RegisterAsic("amin2.js")
#	print GetAddress(pyc.Head, "hier_insdfsdfsdfst")
	GetPage(pyc.Head, 0, file)
else:
	pass

