import math
from functools import reduce
'''
def decodeval(hexvaluetemp, start, end):
	hexvalue = hexvaluetemp[hexvaluetemp.index('x')+1:len(hexvaluetemp):1]
	binaryvalue = (bin(int(hexvalue, 16))[2:]).zfill((len(hexvalue)-1)*4)
	fieldset = []
	fieldclass = 0
	for i in range(len(binaryvalue)):
		if fieldclass < len(start) and (i >= start[fieldclass] and i <= end[fieldclass]):
			fieldset.append(fieldclass+1)
			if i == end[fieldclass]:
				fieldclass += 1
		else:
			fieldset.append(0)
	fieldset.reverse()
	return binaryvalue, fieldset
'''
def decodeval(hexvaluetemp, start, end):
	end = [x+1 for x in end]
	hexvalue = hexvaluetemp[hexvaluetemp.index('x')+1::]
	hexvalue = hexvalue.replace('\r', "")
	hexvalue = hexvalue.replace('\n', "")
	length = (len(hexvalue))*4
#	print hexvalue
#	print ':'.join(x.encode('hex') for x in hexvalue )
#	print type(hexvalue)
#	print len(hexvalue)
#	print length
	binaryvalue = ((bin(int(hexvalue, 16))[2:]).zfill(length))[::-1]
#	print binaryvalue
	splitindex = sorted(list(set([0] + (start + end)+[len(binaryvalue)])))
	bitfields = [binaryvalue[splitindex[i]:splitindex[i+1]] for i in range(len(splitindex)-1)]
	bitfields = [binaryvalue[splitindex[i]:splitindex[i+1]][::-1] for i in range(len(splitindex)-1)[::-1]]
	bitfields = [hex(int(i, 2))[2:].zfill(int(math.ceil(len(i)/4.0))) for i in bitfields]
	bitfieldlabels = []
	c = 1
	for i in range(len(splitindex)-1):
		if splitindex[i] in start:
			bitfieldlabels = [c] + bitfieldlabels
			c += 1
		else:
			bitfieldlabels = [0] + bitfieldlabels
	return bitfields, splitindex[::-1], bitfieldlabels# = bitfields, splitindex[::-1], bitfieldlabels
	#print bitfields
	#print splitindex
	#print bitfieldlabels
	
	#hexfieldlen = len(splitindex)

	#bitfields = [i for i in bitfields if i]
	#hexfields = [ (hex(int(i, 2))[2:]).zfill( int(math.ceil(len(i)/4.0)) ) for i in bitfields if i]
	#numbfields = 1
	#hexfieldlabel = []
	#for i in splitindex[::-1]:
	#	if i in start:
	#		hexfieldlabel = [numbfields] + hexfieldlabel
	#		numbfields += 1			
	#	else:
	#		hexfieldlabel = [0] + hexfieldlabel
	#hexfieldlabel.pop(0)
	#hexfields, hexfieldlabel, splitindex

if __name__ == "__main__":
	print decodeval("0x0000006a", [2], [7])
	print decodeval("0x00220008", [0, 16], [4, 21]) 
	print decodeval("0x00000000", [0], [31])
	print decodeval("0x0000000000000400", [0], [47])
else:
	pass

