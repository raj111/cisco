var ErrorLists = ['Incomplete Input'];
var WebSocket;

function OnSubmit()
{
	//console.log("hi");
	var data;
	var formdata = [];
	var formvalidity = true;

	data = document.getElementById("rddnfld").value;
	if(data == []) {
		document.getElementById("rddntxt").style.color = "red";
		formvalidity = false;
	}
	formdata.push(data);

	data = document.getElementById("rdipfld").value;
	if (!(/^[0-9.]+$/.test(data))) {
		document.getElementById("rdiptxt").style.color = "red";
		formvalidity = false;
	}
	formdata.push(data);

	data = document.getElementById("rdunfld").value;
	if(data == []) {
		document.getElementById("rduntxt").style.color = "red";
		formvalidity = false;
	}
	formdata.push(data);

	data = document.getElementById("rdpwfld").value;
	if(data == []) {
		document.getElementById("rdpwtxt").style.color = "red";
		formvalidity = false;
	}
	formdata.push(data);

	if(formvalidity == false) {
		document.getElementById("contentwarning").innerHTML = ErrorLists[0];
		document.getElementById("contentwarning").style.display = "block";
		setTimeout(HideError, 4000);
	} else {
		//console.log("done");
		console.log(formdata);
		sendMsg(formdata);
	}
}

function HideError() 
{
	document.getElementById("contentwarning").style.display = "none";
	document.getElementById("rddntxt").style.color = "black";
	document.getElementById("rdiptxt").style.color = "black";
	document.getElementById("rduntxt").style.color = "black";
	document.getElementById("rdpwtxt").style.color = "black";
}

function onLoad()
{
	var url = document.URL.replace("http", "ws");
	url = url.replace(url.substring(url.indexOf('/', 5), url.length), "/") + "LogInwebsocket";
	console.log(url);
	WebSocket = new WebSocket(url);
	console.log("[INF] [SOCK] Socket Connection Created");
	WebSocket.onmessage = function (e){recieveMsg(e.data);}
}

function recieveMsg(msg)
{
	if(msg == 'done') {
		window.location="/UI/DCDB";

	}
	else {
		document.getElementById("contentwarning").innerHTML = msg;
		document.getElementById("contentwarning").style.display = "block";
		setTimeout(HideError, 4000);
	}

}

function sendMsg(optxt) 
{
	WebSocket.send(optxt);
}

