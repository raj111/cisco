function createLineElement(x, y, length, angle, thisid) 
{
console.log("got id " + thisid);
    var line = document.createElement("div2");
    var styles = 'border: 1px solid black; '
               + 'width: ' + length + 'px; '
               + 'height: 0px; '
               + '-moz-transform: rotate(' + angle + 'rad); '
               + '-webkit-transform: rotate(' + angle + 'rad); '
               + '-o-transform: rotate(' + angle + 'rad); '  
               + '-ms-transform: rotate(' + angle + 'rad); '  
               + 'position: absolute; '
               + 'top: ' + y + 'px; '
               + 'visibility: visible; '
               + 'left: ' + x + 'px; ';
    line.setAttribute('style', styles); 
	line.setAttribute("id",  String(thisid) + "line"); 
    return line;
}

function createLine(x1, y1, x2, y2, thisid) 
{

    var a = x1 - x2,
        b = y1 - y2,
        c = Math.sqrt(a * a + b * b);

    var sx = (x1 + x2) / 2,
        sy = (y1 + y2) / 2;

    var x = sx - c / 2,
        y = sy;

    var alpha = Math.PI - Math.atan2(-b, a);

    return createLineElement(x, y, c, alpha, thisid);
}



function createline(sx, sy, dx, dy, thisid)
{
	
	document.body.appendChild(createLine(sx, sy, dx, dy, thisid));
}
