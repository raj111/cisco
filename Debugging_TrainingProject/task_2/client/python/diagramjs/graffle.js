Raphael.fn.connection = function (obj1, obj2, line, bg) {

	if (obj1.line && obj1.from && obj1.to) {
		line = obj1;
		obj1 = line.from;
		obj2 = line.to;
    }
    
	var bb1 = obj1.getBBox(),
    bb2 = obj2.getBBox(),
    p = [
			{x: bb1.x + bb1.width / 2, y: bb1.y - 1},
		    {x: bb1.x + bb1.width / 2, y: bb1.y + bb1.height + 1},
		    {x: bb1.x - 1, y: bb1.y + bb1.height / 2},
		    {x: bb1.x + bb1.width + 1, y: bb1.y + bb1.height / 2},
		    {x: bb2.x + bb2.width / 2, y: bb2.y - 1},
		    {x: bb2.x + bb2.width / 2, y: bb2.y + bb2.height + 1},
		    {x: bb2.x - 1, y: bb2.y + bb2.height / 2},
		    {x: bb2.x + bb2.width + 1, y: bb2.y + bb2.height / 2}
		],

    d = {};
	dis = [];

    for (var i = 0; i < 4; i++) {
        for (var j = 4; j < 8; j++) {
            var dx = Math.abs(p[i].x - p[j].x);
            var dy = Math.abs(p[i].y - p[j].y);
            if ((i == j - 4) || (((i != 3 && j != 6) || p[i].x < p[j].x) && ((i != 2 && j != 7) || p[i].x > p[j].x) && ((i != 0 && j != 5) || p[i].y > p[j].y) && ((i != 1 && j != 4) || p[i].y < p[j].y))) {
                dis.push(dx + dy);
                d[dis[dis.length - 1]] = [i, j];
            }
        }
    }

    if (dis.length == 0) {
        var res = [0, 4];
    } 
	else {
        res = d[Math.min.apply(Math, dis)];
    }

    var x1 = p[res[0]].x;
    var y1 = p[res[0]].y;
    var x4 = p[res[1]].x;
    var y4 = p[res[1]].y;

    dx = Math.max(Math.abs(x1 - x4) / 2, 10);
    dy = Math.max(Math.abs(y1 - y4) / 2, 10);

    var x2 = [x1, x1, x1 - dx, x1 + dx][res[0]].toFixed(3);
    var y2 = [y1 - dy, y1 + dy, y1, y1][res[0]].toFixed(3);
    var x3 = [0, 0, 0, 0, x4, x4, x4 - dx, x4 + dx][res[1]].toFixed(3);
    var y3 = [0, 0, 0, 0, y1 + dy, y1 - dy, y4, y4][res[1]].toFixed(3);

    var path = ["M", x1.toFixed(3), y1.toFixed(3), "C", x2, y2, x3, y3, x4.toFixed(3), y4.toFixed(3)].join(",");

    if (line && line.line) {
        line.bg && line.bg.attr({path: path});
        line.line.attr({path: path});
    }
	else{
        var color = typeof line == "string" ? line : "#000";
        return{
            bg: bg && bg.split && this.path(path).attr({stroke: bg.split("|")[0], fill: "none", "stroke-width": bg.split("|")[10] || 10}),
            //line: this.path(path).attr({stroke: color, fill: "none"}),
			line: this.path(path).attr({stroke: color, "stroke-width":10}),
            from: obj1,
            to: obj2
        };
    }
};

	var el;
	window.onload = function (){
    	var dragger = function (){
/*
			if(){
				this.ox = this.type == "rect" ? this.attr("x") : this.attr("cx");
        		this.oy = this.type == "rect" ? this.attr("y") : this.attr("cy");
			}
			else if(){
			

			}
			else{


			}
*/

        	this.ox = (this.type == "rect" || this.type == "image") ? this.attr("x") : this.attr("cx");
        	this.oy = (this.type == "rect" || this.type == "image") ? this.attr("y") : this.attr("cy");

        	//this.ox = this.type == "image" ? this.attr("x") : this.attr("cx");
        	//this.oy = this.type == "image" ? this.attr("y") : this.attr("cy");
        	//this.animate({"fill-opacity": .2}, 500);
			this.animate(200);
    	},
        
		move = function (dx, dy) {
/*
			if(this.type == "rect"){
				var att = this.type == "rect" ? {x: this.ox + dx, y: this.oy + dy} : {cx: this.ox + dx, cy: this.oy + dy};	
			}
			else if(this.type == "image"){
				var att = this.type == "image" ? {x: this.ox + dx, y: this.oy + dy} : {cx: this.ox + dx, cy: this.oy + dy};
			} 
			else{
				var att = this.type == "rect" ? {x: this.ox + dx, y: this.oy + dy} : {cx: this.ox + dx, cy: this.oy + dy};
			}

(this.type == "rect" || this.type == "image")
  */          

			var att = (this.type == "rect" || this.type == "image") ? {x: this.ox + dx, y: this.oy + dy} : {cx: this.ox + dx, cy: this.oy + dy};
            //
            this.attr(att);
            for (var i = connections.length; i--;) {
                r.connection(connections[i]);
            }
            //r.safari();
        },

        up = function () {
            //this.animate({"fill-opacity": 0}, 500);
			this.animate(200);
        },

        r = Raphael("holder", 640, 480),
        connections = [],

        shapes = [  r.image("buffer.png", 500, 120, 120, 120),
                    r.image("cpu2.png", 230, 80, 200, 200),
					r.image("gpio.png", 270, 340, 120, 120),
					r.image("hd.png", 40, 105, 120, 150),
                    //r.ellipse(450, 100, 20, 20),
					
                ];
		
	

    for (var i = 0, ii = shapes.length; i < ii; i++) {
        var color = Raphael.getColor();
		console.log(color);
		if(i==1){
			color = "#33ff3f";
		}
        //shapes[i].attr({fill: color, stroke: color, "fill-opacity": 0, "stroke-width": 2, cursor: "move"});
		shapes[i].attr({fill: color, stroke: color, "stroke-width": 2, cursor: "move"});
        shapes[i].drag(move, dragger, up);
    }

	pathtable = [];
	connections.push(r.connection(shapes[0], shapes[1], "#00f", "#00f|1"));
    connections.push(r.connection(shapes[1], shapes[2], "#0f0", "#0f0|1"));
    connections.push(r.connection(shapes[1], shapes[3], "#f00", "#f00|1"));
	pathtable[connections[0].line.id] = 0;
	pathtable[connections[1].line.id] = 1;
	pathtable[connections[2].line.id] = 2;


	connections[0].line.mouseover(function (e){
			var txt = "H - ";
			txt = txt.concat(pathtable[this.id]).concat(" [").concat(e.clientX).concat(", ").concat(e.clientY).concat(")");			
			console.log(txt);
			document.getElementById("dis").style.left = e.clientX + 20 + "px";
    		document.getElementById("dis").style.top = e.clientY + 20 + "px";
    		document.getElementById("dis").style.visibility = "visible";
			var txt = "   Data Path number : ";
			txt = txt.concat(pathtable[this.id]).concat("<br>").concat("  Data Speed : INF Mbps").concat("<br>").concat("  Packet Drops : 100%");
			document.getElementById("dis").innerHTML = txt;
          });

	connections[0].line.mouseout(function (e){
    	document.getElementById("dis").style.visibility = "hidden"
	});

	connections[1].line.mouseover(function (e){
			var txt = "H - ";
			txt = txt.concat(pathtable[this.id]).concat(" [").concat(e.clientX).concat(", ").concat(e.clientY).concat(")");			
			console.log(txt);
			document.getElementById("dis").style.left = e.clientX + 20 + "px";
    		document.getElementById("dis").style.top = e.clientY + 20 + "px";
    		document.getElementById("dis").style.visibility = "visible";
			var txt = "   Data Path number : ";
			txt = txt.concat(pathtable[this.id]).concat("<br>").concat("  Data Speed : INF Mbps").concat("<br>").concat("  Packet Drops : 100%");
			document.getElementById("dis").innerHTML = txt;
          });

	connections[1].line.mouseout(function (e){
    	document.getElementById("dis").style.visibility = "hidden"
	});


	connections[2].line.mouseover(function (e){
			var txt = "H - ";
			txt = txt.concat(pathtable[this.id]).concat(" [").concat(e.clientX).concat(", ").concat(e.clientY).concat(")");			
			console.log(txt);
			document.getElementById("dis").style.left = e.clientX + 20 + "px";
    		document.getElementById("dis").style.top = e.clientY + 20 + "px";
    		document.getElementById("dis").style.visibility = "visible";
			var txt = "   Data Path number : ";
			txt = txt.concat(pathtable[this.id]).concat("<br>").concat("  Data Speed : INF Mbps").concat("<br>").concat("  Packet Drops : 100%");
			document.getElementById("dis").innerHTML = txt;
          });

	connections[2].line.mouseout(function (e){
    	document.getElementById("dis").style.visibility = "hidden"
	});






	connections[1].line.mouseover(function (e){
			var txt = "H - ";
			txt = txt.concat(pathtable[this.id]).concat(" [").concat(e.clientX).concat(", ").concat(e.clientY).concat(")");			
			console.log(txt);
          });
	connections[2].line.mouseover(function (e){
			var txt = "H - ";
			txt = txt.concat(pathtable[this.id]).concat(" [").concat(e.clientX).concat(", ").concat(e.clientY).concat(")");			
			console.log(txt);
          });
	shapes[0].mouseover(function (){
			console.log("mouse over");
          });	
	//connections.push(r.connection(shapes[0], shapes[4], "#000", "#f00"));

};

