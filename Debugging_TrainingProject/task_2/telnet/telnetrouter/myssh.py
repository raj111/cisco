import paramiko
import time

class Router:
	def __init__(self):
		self.SSHVar = paramiko.SSHClient()
		self.SSHVar.set_missing_host_key_policy(paramiko.AutoAddPolicy())

	def Connect(self):
		try:
			self.SSHVar.connect("10.104.33.159",username='root',password='lab')
		except Exception, e:
			return str(e)
		return "Success"
	
	def BringUp(self):
		try:
			self.ChannelVar = self.SSHVar.invoke_shell()
		except Exception, e:
			return str(e)
		return "Success"

	def Cmd(self, cmd):
		try:
			a = self.ChannelVar.send(cmd + '\n')
			time.sleep(0.1)	
		except Exception, e:
			return str(e)

	def Read(self):
		buff = ''
		while(self.ChannelVar.recv_ready()):
			buff += self.ChannelVar.recv(9999)
		return buff
			

	def Close(self):
		self.SSHVar.close()

if __name__ == "__main__":
	r = Router()
	if r.Connect() == "Success":
		if r.BringUp() == "Success":
			r.Cmd("ls -l")
			r.Cmd("cd /harddisk")
			r.Cmd("ls -l")
			print r.Read()

	r.Close()
else:
	pass

'''
s = paramiko.SSHClient()
s.set_missing_host_key_policy(paramiko.AutoAddPolicy())
s.connect('10.104.33.159', username='root', password='lab')

c = s.invoke_shell()

if( c.recv_ready()):
	out = c.recv(9999)
	print out
else:
	print "no"

c.send('cd /harddisk \n')
time.sleep(0.1)
if( c.recv_ready()):
	out = c.recv(9999)
	print out
else:
	print "no"

c.send('./yoda_ra_static \n')
time.sleep(0.2)
if( c.recv_ready()):
	out = c.recv(9999)
	print out
else:
	print "no"

c.send('read .gtr_csr32.gtr_err_cmd_leaf_int.leaf_halt_en_rw1c \n')
time.sleep(0.2)
if( c.recv_ready()):
	out = c.recv(9999)
	print out
else:
	print "no"

#.gtr_csr
#.gtr_csr32
#cfg_gtr

s.close()

'''
