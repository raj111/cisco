# python script to create tree structure for procyon chip

import re # for getting sting inside "<>"

class Node:
	def __init__(self, name, address, parent):
		self.name = name
		self.address = address
		self.child = []
		self.parent = parent

try:
	file = open('amin.js','r')
except IOError:
	print "checkfile"

head = Node("ProcyonRegisterTree","00000", None)
count = 0
current_level = 0
currentnode = head
for line in file:
	if (count>-1):
		count=count+1
		i=0
		while(line[i]==' '):
			i=i+1
		words = re.findall(r'\"(.+?)\"', line)
		for word in words:
			if("@" in word):
				wr =  word.split()
				name = wr[0]
				address = wr[1]
		i = i/2 - 1
		if(i==current_level):
			print "+++ ", name
			temp = Node(name, address, currentnode)
			currentnode.child.append(temp)
		elif(i>current_level):
			print  currentnode.name," ---> ",currentnode.child[-1].name
			currentnode = currentnode.child[-1]
			current_level = current_level + 1
			print "+++ ", name
			temp = Node(name, address, currentnode)
			currentnode.child.append(temp)
		else:
			while(current_level != i):
				current_level = current_level - 1
				print currentnode.parent.name," <--- ", currentnode.name
				currentnode = currentnode.parent
			print "+++ ", name
			temp = Node(name, address, currentnode)
			currentnode.child.append(temp)
file.close()
